#--------------------------------------------------------#
#-------------0. DECLARACION LIBRERIAS-------------------#
#--------------------------------------------------------#

### --- Limpiar ambiente --- ### 
import os
import re
path_dos = os.getcwd()
path = re.sub(string = re.sub(string=os.getcwd(),pattern=r'\\',repl='/'),pattern='Codigos/Funciones' ,repl='') + '/'
import pandas as pd
import re #Para patrones regulares
import numpy as np
import io
import collections

import textract #lectura rápida
import pdf2image #Lee y convierte el pdf en imagen
from PIL import Image #Edita la imagen
import pytesseract #lee el texto  en forma lenta
import boto3 #Para inscribir el client a Amazon.
import platform



#--------------------------------------------------------#
##-------------1. FUNCIONES DE LECTURA-------------------#
#--------------------------------------------------------#
#------------el path de tesseract en windows-----------------------#
if platform.system() == 'Windows':
    pytesseract.pytesseract.tesseract_cmd = path_dos + '/Tesseract-OCR/tesseract'
#------------ 1.2 Lectura normal-------------------------#
#FUNCION: Lectura de PDF to txt
#OBJETIVO: Convierte palabras en formas generales del español
#INPUT: Vector de string
#OUPUT: Matriz por cada documento que contiene Nombre_doc y Textos
def Lectura_PDF_NORMAL(Contenido,Nombre):
    imageBlobs = pdf2image.convert_from_bytes(Contenido)   
    texto = []
    for page in range(len(imageBlobs)):
        img = imageBlobs[page]
        img = img.resize([img.width*3, img.height*3], Image.ANTIALIAS)
        img = img.convert('L')
        imagetext = pytesseract.image_to_string(img, lang='eng')
        texto.append(imagetext)
    texto = ' '.join(texto)
    Matriz = pd.DataFrame({'nombres_doc': Nombre, 'Textos':[texto],'Tipo_Lectura':2 })
    return(Matriz)

#FUNCION: Limpieza de documentos
#OBJETIVO: Arregla y homogeniza algunos de los caracteres del texto.
#INPUT:  Matriz de documentos que contiene:nombres_doc y Textos.
#OUPUT: Matriz de documentos que contiene:nombres_doc y Textos procesados.
def Limpieza_documentos(documentos):
    #parte de la limpieza, unificar las palabras claves de busqueda:
    documentos.Textos = documentos.Textos.str.replace(r'EMPRE(\w+|\W+)SA',' EMPRESA ')
    documentos.Textos = documentos.Textos.str.replace(r' [Ss]( |\.)?[Ee]( |\.)?[Pp]( |\.)?','SEP ')
    documentos.Textos = documentos.Textos.str.replace(r' [Ll]( |\.)?[Tt]( |\.)?[Dd]( |\.)?',' LTA ')
    documentos.Textos = documentos.Textos.str.replace(r' S( |\.)?[aA]( |\.)?',' SA ')
    documentos.Textos = documentos.Textos.str.replace(r' S( |\.)?[aA]( |\.)? [Cc]( |\.)?',' SAC ')
    documentos.Textos = documentos.Textos.str.replace(r' [Ee]( |\.)?[Ss]( |\.)?[Pp]( |\.)?',' ESP ')
    documentos.Textos = documentos.Textos.str.replace(r'Ã©','e')
    documentos.Textos = documentos.Textos.str.replace(r'ﬁ','ñ')
    documentos.Textos = documentos.Textos.str.replace(r'cién','ción')
    documentos.Textos = documentos.Textos.str.replace(r'(l|I)\\?(’|\')','i')
    documentos.Textos = documentos.Textos.str.replace(r'(L\'|L[’\']J)','ú')
    documentos.Textos = documentos.Textos.str.replace(r'~IA','ÑIA')
    documentos.Textos = documentos.Textos.str.replace(r'[Pp][OoÓóée0][|l]iza','Poliza')
    
    #quitar espacios en blanco
    documentos.Textos = documentos.Textos.str.replace(' +', ' ')#Quitar multple espacios
    documentos.Textos = documentos.Textos.str.replace(r'(\n|\r)+',' ')

    #Lista de encodigs raros
    mpa = dict.fromkeys(range(32))
    documentos.Textos = [x.translate(mpa) for x in documentos.Textos]
    return(documentos)

#--------- 1.2 Lectura Amazon---------------------------#
#FUNCIÓN:   Boto3_cliente
#OBJETIVO: Llama la función de Amazón - Textract
client = boto3.client(
         service_name='textract',
         region_name= 'us-east-1',
         endpoint_url= 'https://textract.us-east-1.amazonaws.com',
)

#FUNCION: Leer imagen amazon
#OBJETIVO: Enviar la imagen a Amazon para lectura en la herramienta textract Amazon.
#INPUT: Imagen en bits
#OUPUT: Respuesta de Amazon
def read_imagen_amazon(imagen_pdf):
    response_total = []
    #recorre cada una de las páginas del documento
    for ii in range(len(imagen_pdf)):
        roiImg = imagen_pdf[ii]
        imgByteArr = io.BytesIO()
        roiImg.save(imgByteArr, format='PNG')
        bytes_test = imgByteArr.getvalue()
        
        response = client.analyze_document(Document={'Bytes': bytes_test},
                                   FeatureTypes=['FORMS'])#['TABLES','FORMS'])
        response_total.append(response)
    return(response_total)

#FUNCION: Imagen a texto
#OBJETIVO: Convierte la respuesta de Amazon a Texto.
#INPUT: Responde de Amazon
#OUPUT:  Vector string, que contiene el texto del documento.
def imagen_text(response_todos):
    textos_escan = ''
    for response in response_todos:
        for item in response["Blocks"]:
            if 'Text' in list(item.keys()):
                textos_escan =  textos_escan + ' ' + item["Text"] 
    return(textos_escan)


#FUNCION: Lectura de pdf a AMAZON
#INPUT: Ubicación de(l) archivo(s)
#OUTPUT: Matriz de pandas, que contiene:Nombre archivo, texto.
def Lectura_PDF_AMAZON(Contenido, Nombre):
    #decodifica el arvhio en bytes
    decoded = base64.b64decode(Contenido)
    #convierte los bytes en imagen
    imageBlobs = pdf2image.convert_from_bytes(decoded)
    #Crea el verctor del texto
    texto = []
    #Recorre el documento por paginas.
    for page in range(len(imageBlobs)):
        #Envia a la funcion de lectura de amazon
        responses_todos = read_imagen_amazon(imageBlobs[page])
        #Extrae el texto de amazon
        textos_pdf = imagen_text(responses_todos)
        #contatena los textos extraidos 
        textos.append(textos_pdf)
    #une los textos de las paginas
    texto = ' '.join(texto)
    #Crea el data frame
    Matriz = pd.DataFrame({'nombre_archivo': [Nombre], 'Textos':[texto],'Tipo_Lectura': 1 })
    return(Matriz)

#----- 1.4 LLama a las funciones de lectura--------------#

#FUNCIÓN: Lectura de datos general
#OBJETIVOS: 
#INPUT:
#OUPUT:
def Lectura_datos_general(Tipo_lectura, Contenido, Nombre):
    ### ------- Normal -------- ###
    if Tipo_lectura == 'Normal': 
        documentos_prev = Lectura_PDF_NORMAL(Contenido = Contenido,Nombre = Nombre)
        documentos = Limpieza_documentos(documentos_prev)
    ### --- Aca va la función de lectura de datos Amazon --- ###
    if Tipo_lectura == 'Amazon': 
        #LLama la funcion de Amazon #
        #Esta función se debe habilitar cuando se tenga el servidor de Amazon- Textract#
        #documentos =  Lectura_PDF_AMAZON(Contenido, Nombre)
        #----Carga los documentos  por ahora----#
        #Cargar documentos
        documentos = pd.read_pickle(path+'/Datos/Polizas_AMAZON_uno.p','gzip')
        documentos = documentos.dropna()
        documentos = documentos.reset_index()
        del documentos['index']
        ####### ---------- Arreglar nombres - Parcial borrar cuando cambie Amazon---------- ####### 
        documentos['nombres_doc'] = documentos['nombres_doc'].apply(lambda x: re.sub(string = re.sub(string =re.sub(string=x,pattern= r'/Users/Usuario/Documents/Proyectosquantil/Aon/Informacion\\',repl=''),pattern= r'.+\\',repl=''),pattern = '.Pdf',repl=''))  
    
    #Elimina el ".Pdf"#
    documentos['nombres_doc'] = documentos['nombres_doc'].apply(lambda x: re.sub( string = x,pattern = '\.[Pp]df',repl=''))
    #__RETORNA MATRIZ__#
    #nombres_doc ; Textos
    return(documentos)


#--------------------------------------------------------#
##-----2. FUNCIONES DE LECTURA DE FAMILIA----------------#
#--------------------------------------------------------#


#----- 2.0 Funciones generales --------------------------#
#FUNCION: Unique
#OBJETIVO: Deja elementos únicos de una lista y conserva el orden
#INPUT: Lista de objetos
#OUPUT: Lista única de objetos
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]

#FUNCION: Evaluacion de Regla
#OBJETIVO: Evalua un patrón de expresiones regulares "regex" en un texto ("documento") bajo unos parámetros "flaggs"
#INPUT: Patrón regular -> "rege", Texto -> "Documento", "parámetros"
#OUPUT: Lista única de objetos
def evaluacion_regla(regex, documento, flaggs):
    matches = re.finditer(regex, documento, flaggs )#re.I|re.M) no se puede usar re.I, porque es n
    valor = []
    for matchNum, match in enumerate(matches, start=1):
        valor.append(match.group())
    return valor

#FUNCION: Español palabras
#OBJETIVO: Convierte palabras en formas generales del español
#INPUT: Vector de string que quiere convertir al español
#OUPUT: Vector de string
def Espanhol_Palabras(lista_palabras):
    lista_palabras = [re.sub(r'(a|á)','[áa]', x, re.I) for x in lista_palabras]
    lista_palabras = [re.sub(r'(e|é)','[eé]', x, re.I) for x in lista_palabras]
    lista_palabras = [re.sub(r'(i|í)','[ií]', x, re.I) for x in lista_palabras]
    lista_palabras = [re.sub(r'(o|ó)','[oó]', x, re.I) for x in lista_palabras]
    lista_palabras = [re.sub(r'(u|ú)','[uú]', x, re.I) for x in lista_palabras]
    return(lista_palabras)

    
#FUNCION: Asegurador de póliza
#OBJETIVO: Busca el asegurador principal de cada una de las polizas, dada un archivo en excel que el usuario debe alimentar.
#INPUT: Vector de texto.
#OUPUT: Valor de string.
def Asegurador_de_poliza(textos):
    if any([bool(re.search(x, textos, re.I)) for x in Tipo_Aseguradora]) == True:
        postition = [re.search(x, textos, re.I) for x in Tipo_Aseguradora]
        postition = [x for x in postition if not pd.isnull(x)]
        conjuntos_tipo= pd.DataFrame({'Posicion' :[x.start()  for x in postition] ,'Nombre' :[x.group()  for x in postition] })
        conjuntos_tipo = conjuntos_tipo.sort_values('Posicion').reset_index(drop=True)
        valor = conjuntos_tipo.Nombre[0]
        return(valor)
    else:
        return 'No lo encontro'
    
#FUNCION: Reconocimiento_aseguradora
#OBJETIVO: Reconoce la aseguradora principal de cada una de las pólizas
#INPUT: Matriz de que contiene, nombre de documento, Texto (output de Lectura_doc_textos)
#OUPUT: Matriz que contiene: Nombre documento, Texto, Aseguradora.
def Reconocimiento_Aseguradora(documentos):
    aseguradoras = []
    for ii in range(documentos.shape[0]):
        aseguradoras.append(Asegurador_de_poliza(documentos.Textos[ii]))
    documentos['Aseguradora']= aseguradoras
    documentos.Aseguradora = documentos.Aseguradora.str.upper()
    return(documentos)


#----- 2.1 FAMILIA HDI --------------------------#
#FUNCION: Caract_Basicas_HDI
#OBJETIVO: Determinar las caracteristicas básicas de las polizas de la aseguradora HDI como : Número de poliza, asegurado, iD_asegurad,
#fecha de vigencia, valor de emisión,tipo de poliza, etc.
#INTPUT: Matriz de polizas identificadas como de HDI
#OUTPUT Matriz con caracteriticas principales ( Número de poliza, asegurado, iD_asegurad,
#fecha de vigencia, valor de emisión,tipo de poliza, etc.)
def Carct_Basicas_HDI(Documents_HDI):
    for ii in range(Documents_HDI.shape[0]):
        #print(ii)
        texto = Documents_HDI.Textos[ii]
        Ubic_pol = Documents_HDI.nombres_doc[ii]
        Aseguradora= Documents_HDI.Aseguradora[ii]
    
        #---------extract numero de poliza, certificado, endoso---------#
        if pd.isna(re.search('(?<=POLIZA ENDOSO CERTIF./DECL. POLIZA DE INCENDIO )([0-9]+| )+',texto)) == False:
            num_poliza_cert  = re.search('(?<=POLIZA ENDOSO CERTIF./DECL. POLIZA DE INCENDIO )([0-9]+| )+',texto).group()
            divsiones = num_poliza_cert.split()
            if len(divsiones) == 1:
                Num_poliza = divsiones[0]
                Certificado = 'No se encontro'
            if len(divsiones) == 2:
                Num_poliza = divsiones[0]
                Certificado = divsiones[1]
            if len(divsiones) == 3:
                Num_poliza = divsiones[0]
                Certificado = divsiones[2]
            Num_poliza_log = ''
            Certificado_log = ''
        else:
            Num_poliza = 'No se encontro'
            Certificado = 'No se encontro'
            Num_poliza_log = 'No se encuentra el patrón estandar definido para la familia.'
            Certificado_log = 'No se encontro'
        #---------Tipo y moneda emitido ---------#
        if pd.isna(re.search('POLIZA DE ([A-Z]+|[ÁÉÍÓÚÑ]| )+', texto)) ==False:
            Tipo = re.search('POLIZA DE ([A-Z]+|[ÁÉÍÓÚÑ]| )+', texto).group()
            Tipo_log = ''
        else:
            Tipo = 'No se encuentro'
            Tipo_log = 'No se encontra el patrón estandar definido para la familia.'
    
        if pd.isna(re.search('(?<=EMITIDA EN)( |:)+([A-Z]+|[ÁÉÍÓÚÑ]|$)+',texto)) == False:
            emitida = re.search('(?<=EMITIDA EN)( |:)+([A-Z]+|[ÁÉÍÓÚÑ]|$)+',texto).group()
            Emitida = re.sub(':','',emitida)
            Emitida_log = ''
        else:
            Emitida = ''
            Emitida_log = 'No se encontra el patrón estandar definido para la familia.'

        #---------Fecha de vigencia ---------#
        if pd.isna(re.search('V(I)?GENCIADESDE (\w+| |:)+[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}\/[0-9]{2}\/[0-9]{4}', 
                         texto)) == False:
            fecha_vigencia = re.search('V(I)?GENCIADESDE (\w+| |:)+[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}\/[0-9]{2}\/[0-9]{4}', 
                         texto).group()
    
            Fecha_vigencia_Desde = re.search('[0-9]{2}\/[0-9]{2}\/[0-9]{4}',fecha_vigencia, re.M).group()
            fecha_vigencia = re.sub(Fecha_vigencia_Desde,'',fecha_vigencia)
            Fecha_vigencia_Hasta = re.search('[0-9]{2}\/[0-9]{2}\/[0-9]{4}',fecha_vigencia, re.M).group()
            Fecha_vigencia_Hasta_log = ''
            Fecha_vigencia_Desde_log = ''
           
        else:
            Fecha_vigencia_Desde = 'No se encontro'
            Fecha_vigencia_Hasta = 'No se encontro'
            Fecha_vigencia_Desde_log = 'No se encontra el patrón estandar definido para la familia.'
            Fecha_vigencia_Hasta_log = 'No se encontra el patrón estandar definido para la familia.'
        
        #--------- Tomador e identificacion del tomador---------#
        if pd.isna(re.search(r'(?<=CONTRATANTE)( |:)+([A-Z]+| |:|[0-9]|[ÁÉÍÓÚÑáéíóú]+|[a-z]+)+' , texto)) == False:
            tomador = re.search(r'(?<=CONTRATANTE)( |:)+([A-Z]+| |:|[0-9]|[ÁÉÍÓÚÑáéíóú]+|[a-z]+)+' , texto).group()
            tomador = re.sub('AON RISK SERVICES','', tomador)
            Tomador = re.sub(r'( |:)+INTERMEDIARIO( |:)+[0-9]+','',tomador)

            regla_identificacion = r'(N(\.)?I(\.)?T(\.)?|C(\.)?C(\.)?|T(\.)?J(\.)?|C(\.)?U(\.)?I(\.)?|R(\.)?U(\.)?C(\.)?|C(\.)?U(\.)?I(\.)?T(\.)?|DOCUMENTO|I(\.)?B(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|D(\.)?N(\.)?I(\.)?|R(\.)?U(\.)?T(\.)?|R(\.)?I(\.)?F(\.)?|S(\.)?A(\.)?T(\.)?|R(\.)?F(\.)?C(\.)?|R(\.)?T(\.)?N(\.)?|R(\.)?T(\.)?U(\.)?|S(\.)?S(\.)?N(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|IDENTIFICACION|R(\.)?U(\.)?N(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|RUC Nro)(\.| |:)(.)+?'
            regla_id_dos = '([0-9]+|\.|\,|\-([A-z]|[0-9]))+'
    
            id_tomador  = re.search(regla_identificacion + regla_id_dos,texto).group()
            ID_Tomador= re.sub(regla_identificacion,'', id_tomador)
            
            Tomador_log = ''
            ID_Tomador_log = ''
            
        else:
            Tomador ='No se encontro'
            ID_Tomador = 'No se encontro'
            Tomador_log =  'No se encontra el patrón estandar definido para la familia.'
            ID_Tomador_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Asegurado e identificacion del Asegurado ---------#
        if pd.isna(re.search(r'(\.|-|[0-9]+)+( )?(([A-Z]+|[ÁÉÍÓÚ&]|([A-Z]+\.)+|([A-Z]+\.[A-Z])+) )+ASEGURADO', texto)) == False:
            asegurado = re.search(r'(\.|-|[0-9]+)+( )?(([A-Z]+|[ÁÉÍÓÚ&]|([A-Z]+\.)+|([A-Z]+\.[A-Z])+) )+ASEGURADO', texto).group()
            ID_Asegurado = re.search(regla_id_dos,asegurado).group() #'([0-9]+|\.|-)+'
            asegurado = re.sub(ID_Asegurado,'',asegurado)
            Asegurado = re.sub('ASEGURADO','',asegurado)
            
            Asegurado_log =  'No se encontra el patrón estandar definido para la familia.'
            ID_Asegurado_log =  'No se encontra el patrón estandar definido para la familia.'
        else:
            Asegurado = 'No se encontro'
            ID_Asegurado='No se encontro'
                    
        #--------- Unir los resultados ---------#

        if ii == 0:
            Base_Inicio_HDI = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta], 
                                   'Moneda_Poliza':[Emitida],
                                  'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                  'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]}) 
            #Matriz el log
            Base_Inicio_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log], 
                                   'Moneda_Poliza':[Emitida_log],
                                  'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                  'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]})   
        else:
            base =  pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta],
                                  'Moneda_Poliza':[Emitida],
                                  'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                  'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]})  
            #Matriz el log
            base_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log], 
                                   'Moneda_Poliza':[Emitida_log],
                                  'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                  'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]}) 
            
            Base_Inicio_HDI = pd.concat([Base_Inicio_HDI,base])
            Base_Inicio_log = pd.concat([Base_Inicio_log,base_log])
            
    return (Base_Inicio_HDI, Base_Inicio_log)
    
    
#FUNCION:Caract_Riesgos_HDI
#OBJETIVO: Identificar los riesgos y sus caracteristicas: Riesgo, ubicación, etc.
#INTPUT:  Matriz de polizas identificadas como de HDI.
#OUTPUT:  1. Matriz con la caracteristicas y descripción de los riesgos en cada póliza.
#         2. Matriz de logs (errores) de riesgos de la póliza.
def Caract_Riesgos_HDI(Documents_HDI):
    regla_pais = ' (C[Oo][Ll][Oo][Mm]Bb][Ii][Aa]|P[eE][rR][uúUÚ]|E[cC][uU][aA][dD][oO][rR]|A[rR][gG][eE][nN][tT][iI][nN][aA]|B[oO][lL][iI][vV][iI][aA]|B[rR][aA][sS][iI][lL]|C[hH][iI][lL][eE]|C[uU][bB][aA]|E[lL] S[aA][lL][vV][aA][dD][oO][rR]|G[uU][aA][tT][eE][Mm][aA][lL][aA]|H[oO][nN][dD][uU][rR][aA][sS]|M[ÉEeé][xX][iI][Cc][oO]|N[iI][cC][aA][rR][aA][gG][uU][aA]|P[aA][nN][aA][mM][aáAÁ]|P[aA][rR][aA][gG][uU][aA][yY]|P[uU][eE][rR][tT][oO] R[iI][cC][oO]|U[rR][uU][gG][uU][aA][yY]|V[eE][nN][eE][zZ][uU][eE][lL][aA])'
    for ii in range(Documents_HDI.shape[0]):
        texto = Documents_HDI.Textos[ii]
        Nombre_POLIZA = Documents_HDI.nombres_doc[ii]
        #--------- Regla de Pais---------#
        if pd.isna(re.search(regla_pais,texto)) == False:
            value_pais = [x.strip(' ') for x in evaluacion_regla(regla_pais, texto, re.I|re.M)]
            value_pais = collections.Counter(value_pais)
            Pais = value_pais.most_common()[0][0]
            Pais_log = ''
        else:
            Pais = 'No determinado'
            Pais_log = 'No se encontra el patrón estandar definido para la familia.'
        
        #--------- Regla de Riesgos y caracteristicas ---------#
        #Declaracion de reglas para riesgos#
        Dircc_riesg = r'DIRECCION DE RIESGO( |:)+([A-Z]+| |[ÁÉÍÓÚÑ])+'
        clase_riesg = r' CLASE( |:)+([A-Z]+| |[0-9]+|[ÁÉÍÓÚÑ])+'
        sub_clase = r'SUB CLASE( |:)+([A-Z]+| |[0-9]+|[ÁÉÍÓÚÑ])+'
        materia_asegurada = r'MATERIA \w+( |:)+([A-Z]+| |\.|[0-9])+'
        
        #--------- reDirección de riesgo ---------#
        if pd.isna(re.search(Dircc_riesg, texto)) == False: 
            DIRC_RIESGO = evaluacion_regla(regex=Dircc_riesg,
                     documento= texto,
                    flaggs= re.M)
            if len(DIRC_RIESGO) > 0 :
                DIRC_RIESGO = [re.sub('COBERTURAS .+','', x, 0, re.M) for x in DIRC_RIESGO]
                DIRC_RIESGO_log =''
            else:
                DIRC_RIESGO =''
                DIRC_RIESGO_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Clase riesgo---------#  
        if pd.isna(re.search(clase_riesg, texto)) == False: 
            CLASE_RIESGO = evaluacion_regla(regex=clase_riesg,
                     documento=texto,
                    flaggs= re.M)
            if len(CLASE_RIESGO) > 0:
                CLASE_RIESGO = [re.sub(' SUB CLASE', '', x) for x in CLASE_RIESGO]
                CLASE_RIESGO_log = ''
            else:
                CLASE_RIESGO = ''
                CLASE_RIESGO_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Sub Riesgo---------#
        if pd.isna(re.search( sub_clase, texto)) == False: 
            SUB_RIESGO = evaluacion_regla(regex=sub_clase,
                     documento=texto,
                    flaggs= re.M)
            if len(SUB_RIESGO) > 0:
                SUB_RIESGO = [re.sub('TIPO DE [A-Z]+', '', x) for x in SUB_RIESGO]
                SUB_RIESGO_log = ''
            else:
                SUB_RIESGO = ''
                SUB_RIESGO_log = 'No se encontra el patrón estandar definido para la familia.'
        
        #--------- Ubicacion de riesgo---------#
        if pd.isna(re.search( materia_asegurada, texto)) == False: 
            UBICACION = evaluacion_regla(regex=materia_asegurada,
                     documento=texto,  flaggs= re.M)
            if len(UBICACION) > 0:
                UBICACION = [re.sub('COBERTURAS .+','', x, 0, re.M) for x in UBICACION]
                UBICACION_log = ''
            else: 
                UBICACION = ''
                UBICACION_log = 'No se encontra el patrón estandar definido para la familia.'
        
        #Unifica la longitud de las listas extraidas#
        if (len(DIRC_RIESGO) == len(UBICACION) == len( CLASE_RIESGO) == len(SUB_RIESGO)) == False:
            longitudes = np.array([len(DIRC_RIESGO) ,len(UBICACION), len( CLASE_RIESGO), len(SUB_RIESGO )])
            #toma el valor maximo
            long_max = max(longitudes)
            #Encuentra las posiciones menores
            posiciones = np.where(longitudes < long_max)
            posiciones = posiciones[0].tolist()
            #amplia su resultado.
            for post in posiciones:
                if post == 0:
                    DIRC_RIESGO = DIRC_RIESGO + ['']*(long_max -len(DIRC_RIESGO))
                if post == 1:
                    UBICACION = UBICACION + ['']*(long_max -len(UBICACION))
                if post == 2:
                    CLASE_RIESGO =  CLASE_RIESGO + ['']*(long_max -len( CLASE_RIESGO ))
                if post == 3:
                    SUB_RIESGO = SUB_RIESGO + ['']*(long_max -len(SUB_RIESGO))
        
        #construye la matrix#
        if ii == 0:
            Base_Riesgo_HDI = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA]*len(DIRC_RIESGO), 'Riesgo_ID': DIRC_RIESGO,
                                        'Direccion_Riesgo':UBICACION , 
                                         'Pais':[Pais]*len(DIRC_RIESGO),
                                        'Descrip_Riesg_1':CLASE_RIESGO[:], 'Descrip_Riesg_2':SUB_RIESGO,
                                         #Segementación de la ubicación : Trabajo Futuro.
                                         'Division_Administrativa_1':['']*len(DIRC_RIESGO),
                                         'Division_Administrativa_2': ['']*len(DIRC_RIESGO),
                                         'Division_Administrativa_3': ['']*len(DIRC_RIESGO),
                                         'Codigo_Postal': ['']*len(DIRC_RIESGO),
                                        'Latitud': ['']*len(DIRC_RIESGO), 'Longitud':['']*len(DIRC_RIESGO)
                                           })
            #Matriz el log
            Base_Riesgo_HDI_log = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 'Riesgo_ID': DIRC_RIESGO_log,
                                        'Direccion_Riesgo': [UBICACION_log] , 
                                         'Pais':[Pais_log],
                                        'Descrip_Riesg_1': [CLASE_RIESGO_log], 'Descrip_Riesg_2': [SUB_RIESGO_log],
                                         #Segementación de la ubicación : Trabajo Futuro.
                                        'Division_Administrativa_1': [''] ,'Division_Administrativa_2': [''],'Division_Administrativa_3': [''],
                                         'Codigo_Postal': [''], 'Latitud': [''], 'Longitud':['']
                                           })
        else:
            base_riesgo = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA]*len(DIRC_RIESGO), 'Riesgo_ID': DIRC_RIESGO,
                                        'Direccion_Riesgo':UBICACION , 
                                         'Pais':[Pais]*len(DIRC_RIESGO),
                                        'Descrip_Riesg_1':CLASE_RIESGO, 'Descrip_Riesg_2':SUB_RIESGO ,
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Division_Administrativa_1':['']*len(DIRC_RIESGO),
                                         'Division_Administrativa_2': ['']*len(DIRC_RIESGO),
                                         'Division_Administrativa_3': ['']*len(DIRC_RIESGO),
                                         'Codigo_Postal': ['']*len(DIRC_RIESGO),
                                        'Latitud': ['']*len(DIRC_RIESGO), 'Longitud':['']*len(DIRC_RIESGO)
                                       })
            #Matriz el log
            base_riesgo_log = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 'Riesgo_ID': DIRC_RIESGO_log,
                                        'Direccion_Riesgo': [UBICACION_log] , 
                                         'Pais':[Pais_log],
                                        'Descrip_Riesg_1': [CLASE_RIESGO_log], 'Descrip_Riesg_2': [SUB_RIESGO_log],
                                         #Segementación de la ubicación : Trabajo Futuro.
                                        'Division_Administrativa_1': [''], 'Division_Administrativa_2': [''],'Division_Administrativa_3': [''],
                                         'Codigo_Postal': [''], 'Latitud': [''], 'Longitud':['']
                                           })
            
            Base_Riesgo_HDI = pd.concat([Base_Riesgo_HDI,base_riesgo])
            Base_Riesgo_HDI_log = pd.concat([Base_Riesgo_HDI_log, base_riesgo_log])
            
        Base_Riesgo_HDI = Base_Riesgo_HDI.drop_duplicates().reset_index(drop =True)
        Base_Riesgo_HDI_log = Base_Riesgo_HDI_log.drop_duplicates().reset_index(drop = True)
    return(Base_Riesgo_HDI, Base_Riesgo_HDI_log)

#FUNCION: Caract_Coberturas_HDI
#OBJETIVO:  Identifica la(s) cobertura(s) por cada uno de los riegos contenidos en las polizas. 
#INTPUT:  Matriz de polizas identificadas como de HDI.
#OUTPUT: 1. Matriz de coberturas, valor por cobertura en cada una de las polizas y riesgo asociado.
#        2. Matriz de logs (errores) de coberturas.
def Caract_Coberturas_HDI(Documents_HDI):
    for ii in range(Documents_HDI.shape[0]):
        texto = Documents_HDI.Textos[ii]
        Nombre_POLIZA = Documents_HDI.nombres_doc[ii]
         #--------- Tipo de moneda ---------#
        if pd.isna(re.search('(?<=EMITIDA EN)( |:)+([A-Z]+|[ÁÉÍÓÚÑ]|$)+',texto)) == False:
            emitida = re.search('(?<=EMITIDA EN)( |:)+([A-Z]+|[ÁÉÍÓÚÑ]|$)+',texto).group()
            Emitida = re.sub(':','',emitida)
            Emitida_log =  ['']
        else:
            Emitida =  ['']
            Emitida_log = 'No se encontra el patrón estandar definido para la familia.'
        #--------- Regla determinar riesgo ---------#
        Dircc_riesg = r'DIRECCION DE RIESGO( |:)+([A-Z]+| |[ÁÉÍÓÚÑ])+'
        DIRC_RIESGO = evaluacion_regla(regex=Dircc_riesg,  documento= texto, flaggs= re.M)
        if len(DIRC_RIESGO) > 0:
            DIRC_RIESGO_log =  ['']
        else:
            DIRC_RIESGO = ['']
            DIRC_RIESGO_log = 'No se encontra el patrón estandar definido para la familia.'
        #1. Sacar las secciones-----------------
        coberturas = ['']
        monto =  ['']
        coberturas_log ='No se encontra el patrón estandar definido para la familia.'
        monto_log = 'No se encontra el patrón estandar definido para la familia.'
        
        TEXT_COBERTURAS = evaluacion_regla( r'COBERTURAS MONTO ASEGURADO PRIMA NETA DEDUCIBLE( HDI Seguros [0-9])?([A-Z]+| |[0-9]+|\.|,|/|\(|\)|eguros){1,}',
                                   texto, re.M)
        if len(TEXT_COBERTURAS) > 0:
            TEXT_COBERTURAS = unique(TEXT_COBERTURAS)
            TEXT_COBERTURAS = [text_c for text_c in TEXT_COBERTURAS if ('REFERIRSE A ESTOS NUMEROS POLIZA ENDOSO' in text_c)== False]
            TEXT_COBERTURAS = [re.sub(r'HDI( SEGUROS| S)?','',x) for x in TEXT_COBERTURAS]
            TEXT_COBERTURAS = [re.sub(r'DESCUENTO AJUSTE( |[A-Z]+|[0-9]+|\.|\,)+','',x) for x in TEXT_COBERTURAS]
            TEXT_COBERTURAS = [re.sub(r'COBERTURAS MONTO ASEGURADO PRIMA NETA DEDUCIBLE','',x) for x in TEXT_COBERTURAS]
            TEXT_COBERTURAS_log = ''
            #3. convertir en tabla-------------------
            Cobert_mont = evaluacion_regla('[A-Z]([A-Z]+(\.)?| )+([0-9]+\.?,?)+', TEXT_COBERTURAS[0], re.M)
            if len(Cobert_mont) > 0:
                coberturas = [re.search('([A-Z]+(\.)?| )+', x, re.M).group() for x in Cobert_mont]
                monto      = [re.search('([0-9]+\.?,?)+', x, re.M).group() for x in Cobert_mont]
                coberturas_log =  ['']
                monto_log =  ['']
            

        if ii == 0:
            Base_Cobert_HDI = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA]*len(coberturas), 
                                        'Cobertura':coberturas, 'Valor_Cobert': monto,'Moneda_Valor_Cobertura':[Emitida]*len(coberturas),
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%': ['']*len(coberturas), 'Base_Valor_Cobertura_%' : ['']*len(coberturas),'Valor_Cobertura_Lucro_cesante_(dias)': ['']*len(coberturas),
                                        'Prima_Cobertura': ['']*len(coberturas), 'Moneda_Prima_Cobertura': ['']*len(coberturas)
                                        })
            Base_Cobert_HDI_log = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 
                                        'Cobertura':coberturas_log, 'Valor_Cobert': monto_log,'Moneda_Valor_Cobertura':[Emitida_log],
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%': [''], 'Base_Valor_Cobertura_%' : [''],'Valor_Cobertura_Lucro_cesante_(dias)': [''],
                                        'Prima_Cobertura': [''], 'Moneda_Prima_Cobertura': ['']
                                        })
            
            
        else:
            base_cobert = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA]*len(coberturas), 
                                        'Cobertura':coberturas, 'Valor_Cobert': monto ,'Moneda_Valor_Cobertura':[Emitida]*len(coberturas),
                                         #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%':'', 'Base_Valor_Cobertura_%' : '' ,'Valor_Cobertura_Lucro_cesante_(dias)':'',
                                        'Prima_Cobertura': '' , 'Moneda_Prima_Cobertura':''
                                        })
            base_cobert_log= pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 
                                        'Cobertura':coberturas_log, 'Valor_Cobert': monto_log,'Moneda_Valor_Cobertura':[Emitida_log],
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%': [''], 'Base_Valor_Cobertura_%' : [''],'Valor_Cobertura_Lucro_cesante_(dias)': [''],
                                        'Prima_Cobertura': [''], 'Moneda_Prima_Cobertura': ['']
                                        })
            
            Base_Cobert_HDI = pd.concat([Base_Cobert_HDI,base_cobert])
            Base_Cobert_HDI_log = pd.concat([Base_Cobert_HDI_log, base_cobert_log])
    
    return(Base_Cobert_HDI,Base_Cobert_HDI_log )



#FUNCION:Caract_Generales_HDI_dos
#OBJETIVO: Resultados de la extraccipón de los valores: Deducible, Lucrocesante y Limites
#INTPUT: Matriz de polizas identificadas como de HDI.
#OUTPUT:1.  Matriz con las caracteristicas de valor asociadas a cada poliza (total asegurado, Total asegurable, Prima total)
#       2.  Matriz de LOG de las caracteristica de valor asociadadas.
def Caract_Generales_HDI_Dos(Documents_HDI):
    for ii in range(Documents_HDI.shape[0]):
        Nombre_Poliza = Documents_HDI.nombres_doc[ii]
        texto = Documents_HDI.Textos[ii]
        #--------- Deducibles ---------#
        #En las polizas de muestra inicial, no se encuentra este patrón#
        Deducible = 'No se encontro'
        Deducible_log = 'No hay regla definida para este item.'
        
        #--------- Lucro cesante ---------#
        if pd.isna(re.search('(Perjuicio por paralizaci[óo]n| p[eé]rdida de beneficio|Lucro Cesante)', texto)) == False:
            Lucro_Cesante = re.search('(Perjuicio por paralizaci[óo]n| p[eé]rdida de beneficio|Lucro Cesante)','texto',re.I).group()
            Lucro_Cesante_log = ''
        else:
            Lucro_Cesante = 'Lucro Cesante: No se encontro'
            Lucro_Cesante_log  = 'Lucro Cesante: No se encontra el patrón estandar definido para la familia.'
            
        #--------- Limites ---------#
        Limites =  evaluacion_regla(r'(?<=TOTAL ASEGURADO)( |:)+([0-9]+|,|\.)+', texto ,re.M)
        if len(Limites) > 0:
            Limites = Limites[0]
            Limites = re.sub(':','', Limites) 
            Limites_log = ''
        else:
            Limites = ''
            Limites_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Participacion/Coaseguradoras ---------#
        if pd.isna(re.search('POLIZA HA SIDO CONTRATADA EN COASEGURO ENTRE LAS', texto)) == False:
            texto_coasegura = texto[re.search('POLIZA HA SIDO CONTRATADA EN COASEGURO ', texto).span()[0]:re.search('Detalle de Participaci',texto).span()[0]]
            coasegura = evaluacion_regla('[0-9]+\)([A-Z]+| |\.)+([0-9]+\.?)+%', texto_coasegura, re.M)[0]
            Participacion = re.search('([0-9]+\.?)+%',coasegura).group()
            Participacion_log = ''
        else:
            Participacion = 'No se encontro'
            Participacion_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Construccion de matrices---------#
        if ii == 0:
            Base_General = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                         #----------------#
                                         'Cobertura': [Lucro_Cesante],
                                         'Limites_raw': [Limites], 'Participacion': [Participacion] })
            
            Base_General_log = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible_log], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': ['No existe regla'],'Base_Deducible_%': ['No existe regla'], 'Deducible_$': ['No existe regla'] ,'Moneda_Deducible_$' : ['No existe regla'],
                                         'Deducible_Lucro_Cesante_(dias)': ['No existe regla'], 'Deducible_Min_$': ['No existe regla'], 'Moneda_Deducible_Min_$': ['No existe regla'],
                                         #----------------#
                                         'Lucro_cesante_raw': [Lucro_Cesante_log],
                                         'Limites_raw': [Limites_log], 'Participacion': [Participacion_log] })
            
            
            
        else: 
            base_general = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza],  
                                          'Deducibles_raw' : [Deducible],  
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                         #----------------#
                                         'Cobertura': [Lucro_Cesante],
                                         'Limites_raw': [Limites], 'Participacion': [Participacion] })
            
            base_general_log = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible_log], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': ['No existe regla'],'Base_Deducible_%': ['No existe regla'], 'Deducible_$': ['No existe regla'] ,
                                         'Moneda_Deducible_$' : ['No existe regla'], 'Deducible_Lucro_Cesante_(dias)': ['No existe regla'], 
                                        'Deducible_Min_$': ['No existe regla'], 'Moneda_Deducible_Min_$': ['No existe regla'],
                                         #----------------#
                                         'Lucro_cesante_raw': [Lucro_Cesante_log],
                                         'Limites_raw': [Limites_log], 'Participacion': [Participacion_log] })
            Base_General = pd.concat([ Base_General , base_general])
            Base_General_log = pd.concat([ Base_General_log , base_general_log])
            
        
    return(Base_General,  Base_General_log )

#FUNCION:Caract_Generales_HDI
#OBJETIVO: Resultados de la extraccipón de los valores: Total asegurado, Total aseguralbe,Prima total
#INTPUT: Matriz de polizas identificadas como de HDI.
#OUTPUT: Matriz con las caracteristicas de valor asociadas a cada poliza (total asegurado, Total asegurable, Prima total)
def Caract_Generales_HDI(Documents_HDI):
    for ii  in range(Documents_HDI.shape[0]):
        texto = Documents_HDI.Textos[ii]
        Nombre_Poliza = Documents_HDI.nombres_doc[ii]
        
        #--------- Tipo de moneda emitida ---------#
        if pd.isna(re.search('(?<=EMITIDA EN)( |:)+([A-Z]+|[ÁÉÍÓÚÑ]|$)+',texto)) == False:
            emitida = re.search('(?<=EMITIDA EN)( |:)+([A-Z]+|[ÁÉÍÓÚÑ]|$)+',texto).group()
            Emitida = re.sub(':','',emitida)
            Emitida_log = ''
        else:
            Emitida = ''
            Emitida_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Total asegurado ---------#
        if pd.isna(re.search(r'(?<=TOTAL ASEGURADO)( |:)+([0-9]+|,|\.)+', texto)) == False:
            total_asegurado = evaluacion_regla(r'(?<=TOTAL ASEGURADO)( |:)+([0-9]+|,|\.)+', texto ,re.M)[0]
            Total_Asegurado = re.sub(':','',total_asegurado) 
            Total_Asegurado_log = ''
        else:
            Total_Asegurado =  ''
            Total_Asegurado_log ='No se encontra el patrón estandar definido para la familia.'
            
        #--------- Total asegurable---------#
        total_asegurable = evaluacion_regla(r'(TOTAL ASEGURABLE|SUMA ASEGURABLE|ASEGURABLE)( |:)+([0-9]+|,|\.)+', 
                                    texto,re.M)
        if len(total_asegurable) > 0:
            total_asegurable = [re.sub('(:| )+','',x) for x in total_asegurable]
            Total_Asegurable = total_asegurable[0]
            Total_asegurable_log = ''
        else:
            Total_Asegurable = re.sub(':','',total_asegurado) 
            Total_Asegurable_log = 'Se asume igual a Total Asegurable' 
    
        #--------- Prima total---------#
        if pd.isna(re.search(r'PRIMA TOTAL( |HDI|Seguros)+?( |[0-9]+\.[0-9]+)+[0-9]', texto, re.M)) == False:
            prima_total = re.search(r'PRIMA TOTAL( |HDI|Seguros)+?( |[0-9]+\.[0-9]+)+[0-9]', texto, re.M).group()
            prima_total = evaluacion_regla(' [0-9]+\.[0-9]+',prima_total, re.M)
            prima_total = prima_total[len(prima_total)-1]
            Prima_Total = prima_total.split()[0]
            Prima_Total_log = ''
        else:
            Prima_Total = ''
            Prima_Total_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Construccion de las matrices  ---------#
        if ii == 0:
            Base_Genral_HDI = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado],
                            'Total_Asegurable': [Total_Asegurable], 'Total_Prima': [Prima_Total],
                            'Moneda_Prima':[Emitida], 'Moneda_Total_Asegurado':[Emitida],'Moneda_Total_Asegurable':[Emitida]
                           })
            
            Base_Genral_HDI_log = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado_log],
                            'Total_Asegurable': [Total_Asegurable_log], 'Total_Prima': [Prima_Total_log],
                            'Moneda_Prima':[Emitida_log], 'Moneda_Total_Asegurado':[Emitida_log],'Moneda_Total_Asegurable':[Emitida_log]
                           })
            
        else:
            base = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado],
                            'Total_Asegurable': [Total_Asegurable], 'Total_Prima': [Prima_Total],
                            'Moneda_Prima':[Emitida], 'Moneda_Total_Asegurado':[Emitida],'Moneda_Total_Asegurable':[Emitida]
                           })
            
            base_log =  pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado_log],
                            'Total_Asegurable': [Total_Asegurable_log], 'Total_Prima': [Prima_Total_log],
                            'Moneda_Prima':[Emitida_log], 'Moneda_Total_Asegurado':[Emitida_log],'Moneda_Total_Asegurable':[Emitida_log]
                           })
            Base_Genral_HDI = pd.concat([Base_Genral_HDI , base])
            Base_Genral_HDI_log = pd.concat([Base_Genral_HDI_log , base_log], sort=False)

    return(Base_Genral_HDI, Base_Genral_HDI_log)

#----- 2.2 FAMILIA SURA--------------------------#

#FUNCION: Caract_Basicas_SURA
#OBJETIVO: Determinar las caracteristicas básicas de las polizas de la aseguradora HDI como : Número de poliza, asegurado, iD_asegurad,
#fecha de vigencia, valor de emisión,tipo de poliza, etc.
#INTPUT: Matriz de polizas identificadas como de SURA
#OUTPUT 1. Matriz con caracteriticas principales ( Número de poliza, asegurado, iD_asegurad,
#          fecha de vigencia, valor de emisión,tipo de poliza, etc.)
#       2. Matriz de LOGS con las caracteristicas principales.
#FUNCION: Caract_Basicas_SURA
#OBJETIVO: Determinar las caracteristicas básicas de las polizas de la aseguradora HDI como : Número de poliza, asegurado, iD_asegurad,
#fecha de vigencia, valor de emisión,tipo de poliza, etc.
#INTPUT: Matriz de polizas identificadas como de SURA
#OUTPUT Matriz con caracteriticas principales ( Número de poliza, asegurado, iD_asegurad,
#fecha de vigencia, valor de emisión,tipo de poliza, etc.)
def Carct_Basicas_SURA(Documents_SURA):
    for ii in range(Documents_SURA.shape[0]):
        texto = Documents_SURA.Textos[ii]
        Ubic_pol = Documents_SURA.nombres_doc[ii]
        Aseguradora= Documents_SURA.Aseguradora[ii]
        #--------- Numero de poliza ---------#
        if pd.isna(re.search('(?<=POLIZA)( |:)+[0-9]+',texto)) == False:
            Nm_poliza =  re.search('(?<=POLIZA)( |:)+[0-9]+',texto).group()
            Num_poliza = re.sub('( |:)+','', Nm_poliza)
            Num_poliza_log = ''
        else:
            Nm_poliza = ''
            Num_poliza_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Certificado ---------#
        if pd.isna(re.search('Certificado(:| )+[0-9]+',texto,re.I)) == False:
            Certificado = re.search('Certificado(:| )+[0-9]+',texto,re.I).group()
            Certificado = re.sub(':','', Certificado)
            Certificado_log = ''
        else:
            Certificado = ''
            Certificado_log  =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Tipo de poliza ---------#
        if pd.isna(re.search(r'RAMO(:| )+([A-Z]|[ÁÉÍÓÚÑ]|\.|,| )+',texto)) == False:
            Tipo = re.search(r'RAMO(:| )+([A-Z]|[ÁÉÍÓÚÑ]|\.|,| )+',texto).group()
            Tipo = re.sub('( )+HASTA','',Tipo)
            Tipo = re.sub('RAMO(:| )+','',Tipo)
            Tipo_log = ''
        else:
            Tipo = ''
            Tipo_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Fecha de vigencia ---------#
        if pd.isna(re.search('(DESDE|HASTA)( |:)+([A-Za-z]+| |[0-9]|\.)+[0-9]{1,2}/[0-9]{1,2}\/[0-9]+',texto)) == False:
            fecha_vigencia = unique(evaluacion_regla('(DESDE|HASTA)( |:)+([A-Za-z]+| |[0-9]|\.)+[0-9]{1,2}/[0-9]{1,2}\/[0-9]+', 
                 texto, re.M))
            Fecha_vigencia_Desde = re.search('[0-9]{1,2}/[0-9]{1,2}\/[0-9]+', fecha_vigencia[0]).group()
            Fecha_vigencia_Hasta = re.search('[0-9]{1,2}/[0-9]{1,2}\/[0-9]+', fecha_vigencia[1]).group()
            Fecha_vigencia_Desde_log = ''
            Fecha_vigencia_Hasta_log = ''
        else:
            Fecha_vigencia_Desde = 'No se encontro'
            Fecha_vigencia_Desde_log =  'No se encontra el patrón estandar definido para la familia.'
            Fecha_vigencia_Hasta = 'No se encontro'
            Fecha_vigencia_Hasta_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Regla de identificacion  de Tomador ---------#
        regla_identificacion = r'(N(\.)?I(\.)?T(\.)?|C(\.)?C(\.)?|T(\.)?J(\.)?|C(\.)?U(\.)?I(\.)?|R(\.)?U(\.)?C(\.)?|C(\.)?U(\.)?I(\.)?T(\.)?|DOCUMENTO|I(\.)?B(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|D(\.)?N(\.)?I(\.)?|R(\.)?U(\.)?T(\.)?|R(\.)?I(\.)?F(\.)?|S(\.)?A(\.)?T(\.)?|R(\.)?F(\.)?C(\.)?|R(\.)?T(\.)?N(\.)?|R(\.)?T(\.)?U(\.)?|S(\.)?S(\.)?N(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|IDENTIFICACION|R(\.)?U(\.)?N(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|RUC Nro)'

        if pd.isna(re.search('(?<=IDENTIFICACION DEL CLIENTE) [A-Z]+( |:)+([A-Z]|[ÁÉÍÓÚÑ]|\.|,)+', texto)) == False:
            tomador = re.search(r'(?<=IDENTIFICACION DEL CLIENTE) [A-Z]+( |:)+([A-Z]|[ÁÉÍÓÚÑ]|\.|,)+' , texto).group()
            Tomador = re.sub('([A-Z]+| )+:','',tomador)
            id_tomador = re.search(regla_identificacion + '(:| )+([0-9]+\.?\,?)+(( |-)+([0-9]+|[A-z]+))?',texto).group()
            ID_Tomador = re.sub('([A-Z]+| )+:','',id_tomador)
            Tomador_log = ''
            ID_Tomador_log = ''
        else:
            Tomador = 'No se encontro'
            Tomador_log =  'No se encontra el patrón estandar definido para la familia.'
            ID_Tomador = 'No se encontro'
            ID_Tomador_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Regla de identificacion de Asegurado ---------#
        if pd.isna(re.search('(?<= ASEGURADO)( |:)+([A-Za-z]+|\.| )+',texto)) == False:
            asegurado = re.search(r'(?<= ASEGURADO)( |:)+([A-Za-z]+|\.| )+', texto).group()
            id_aseg = '(?<=' +asegurado+')( |:)*([0-9]+\\.?\\,?)+(( |-)+?([0-9]+|[A-z]+))?'
            ID_Asegurado = 'No se encontro'
            ID_Asegurado_log =  'No se encontra el patrón estandar definido para la familia.'
            if pd.isna(re.search(id_aseg, texto)) == False:
                id_asegurado = re.search(id_aseg, texto).group()
                id_asegurado = re.sub('(:| )+','',id_asegurado)
                Asegurado = re.sub(r'(( )?:( )?|( )?RUT)+','',asegurado)
                ID_Asegurado = id_asegurado
                Asegurado_log = ''
                ID_Asegurado_log = ''
        else:
            Asegurado = 'No se encontro'
            ID_Asegurado = 'No se encontro'
            Asegurado_log =  'No se encontra el patrón estandar definido para la familia.'
            ID_Asegurado_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Tipo de moneda emitida ---------#
        if pd.isna(re.search('(?<=MONEDA)( |:)+( |[A-Z]+)+',texto)) == False:
            emitida = re.search('(?<=MONEDA)( |:)+( |[A-Z]+)+',texto).group()
            emitida = re.sub(' IVA','',emitida)
            Emitida = re.sub(':','',emitida)
            Emitida_log = ''
        else:
            Emitida = ''
            Emitida_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Construccion de las matrices ---------#  
        if ii == 0:
            Resultados_poliza = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta], 
                                   'Moneda_Poliza':[Emitida],
                                  'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                  'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]})  
            
            Resultados_poliza_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log], 
                                   'Moneda_Poliza':[Emitida_log],
                                  'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                  'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]})  
        else: 
            resultados =  pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta], 
                                   'Moneda_Poliza':[Emitida],
                                  'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                  'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]}) 
            
            resultados_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log], 
                                   'Moneda_Poliza':[Emitida_log],
                                  'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                  'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]}) 
            
            Resultados_poliza =  pd.concat([Resultados_poliza  ,resultados])
            Resultados_poliza_log =  pd.concat([Resultados_poliza_log  ,resultados_log])
    return(Resultados_poliza , Resultados_poliza_log)


#FUNCION: Caracteristicas de Riesgos y Coberturas SURA
#OBJETIVO: Identifica la(s) cobertura(s) por cada uno de los riegos contenidos en las polizas. 
#INTPUT: Matriz de polizas identificadas como de SURA.
#OUTPUT: 1. Matriz de coberturas, valor por cobertura en cada una de las polizas y riesgo asociado.
#        2. Matriz de LOGS de coberturas y sus valores relacionados.
def Caract_Riesgo_Coberturas_SURA(Documents_SURA):
    for ii in range(Documents_SURA.shape[0]):
        texto = Documents_SURA.Textos[ii]
        Nombre_Poliza = Documents_SURA.nombres_doc[ii]
        
        #--------- Regla Pais ---------#
        regla_pais = ' (C[Oo][Ll][Oo][Mm]Bb][Ii][Aa]|P[eE][rR][uúUÚ]|E[cC][uU][aA][dD][oO][rR]|A[rR][gG][eE][nN][tT][iI][nN][aA]|B[oO][lL][iI][vV][iI][aA]|B[rR][aA][sS][iI][lL]|C[hH][iI][lL][eE]|C[uU][bB][aA]|E[lL] S[aA][lL][vV][aA][dD][oO][rR]|G[uU][aA][tT][eE][Mm][aA][lL][aA]|H[oO][nN][dD][uU][rR][aA][sS]|M[ÉEeé][xX][iI][Cc][oO]|N[iI][cC][aA][rR][aA][gG][uU][aA]|P[aA][nN][aA][mM][aáAÁ]|P[aA][rR][aA][gG][uU][aA][yY]|P[uU][eE][rR][tT][oO] R[iI][cC][oO]|U[rR][uU][gG][uU][aA][yY]|V[eE][nN][eE][zZ][uU][eE][lL][aA])'
        if pd.isna(re.search(regla_pais,texto)) == False:
            value_pais = [x.strip(' ') for x in evaluacion_regla(regla_pais, texto, re.I|re.M)]
            value_pais = collections.Counter(value_pais)
            Pais = value_pais.most_common()[0][0]
            Pais_log = ''
        else:
            Pais = 'No se encontro'
            Pais_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Tipo de moneda emitida ---------#
        if pd.isna(re.search('(?<=MONEDA)( |:)+( |[A-Z]+)+',texto)) == False:
            emitida = re.search('(?<=MONEDA)( |:)+( |[A-Z]+)+',texto).group()
            emitida = re.sub(' IVA','',emitida)
            Emitida = re.sub(':','',emitida)
            Emitida_log = ''
        else:
            Emitida = 'No se encontro'
            Emitida_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Regla determinar riesgo ---------#
        ind_riesgo =  re.finditer('NRO UBICACI[ÓO]N\.\s+?[0-9]+', texto)
        ind_riesgo_start= [m.start(0) for m in ind_riesgo]
    
        if len(ind_riesgo_start) > 0:
            #Saca los textos de los riesgos
            Texto_Riesgo = []
            for num in range(len(ind_riesgo_start)):
                if pd.isna(re.search('Total Item [0-9]+', texto[ind_riesgo_start[num]:])) == False:
                    ind_cobert_end =  re.search('Total Item [0-9]+', texto[ind_riesgo_start[num]:]).span()[1]
                    Texto_Riesgo.append(texto[ind_riesgo_start[num]: ind_riesgo_start[num] + ind_cobert_end  ])

        Texto_Riesgo = unique(Texto_Riesgo)
        #Extraer del texto:Riesgo, ubicación riesgo, coberturas, y lucro cesante.
        Dircc_riesgo = ['No se encontro']
        text_cobert = ['No se encontro']
        Num_Riesgo = ['No se encontro']
        Dircc_riesgo = ['No se encontro']
        Clase_riesgo = ['No se encontro']
        Sub_clase =  ['No se encontro']
        Lucro_Cesante = ['Lucro cesate: No se encontro']
        #Los logs encontrados
        num_riesgo_log = 'No se encontra el patrón estandar definido para la familia.'
        dircc_riesgo_log = 'No se encontra el patrón estandar definido para la familia.'
        clase_riesgo_log = 'No se encontra el patrón estandar definido para la familia.'
        sub_clase_log = 'No se encontra el patrón estandar definido para la familia.'
        nombre_cober_log ='No se encontra el patrón estandar definido para la familia.'
        Monto_Cober_log = 'No se encontra el patrón estandar definido para la familia.'
        monto_cober_log = 'No se encontra el patrón estandar definido para la familia.'
            
        if len(Texto_Riesgo) > 0:
            #Los logs encontrados
            num_riesgo_log = ''
            dircc_riesgo_log = ''
            clase_riesgo_log = ''
            sub_clase_log = ''
            nombre_cober_log =''
            Monto_Cober_log = ''
            monto_cober_log = ''
            Lucro_Cesante_log = ''     

            #Encontrar las caracteristicas
            text_cobert = []
            Num_Riesgo = []
            Dircc_riesgo = []
            Clase_riesgo = []
            Sub_clase =  []
            Lucro_Cesante = []
            for text_riesg in Texto_Riesgo:
                num = re.search('(?<=NRO UBICACI[ÓO]N\. )[0-9]+', text_riesg).group()
                item = re.search('(?<=Total Item )[0-9]+', text_riesg).group()

                if num == item:
                    num_riesgo = re.search('NRO UBICACI[ÓO]N\.( |[0-9]+)+', text_riesg).group()
                    Num_Riesgo.append(num_riesgo)

                    dircc_riesgo = re.search('Direcci[óo]n( |:)+([A-Z]+|[0-9]| )+', text_riesg).group()
                    dircc_riesgo = re.sub('Direcci[óo]n(:| )+','',dircc_riesgo)
                    Dircc_riesgo.append(dircc_riesgo )

                    clase_riesgo = re.search('Comuna( |:)+([A-z]+|[ÁÉÍÓÚáéíúó]| )+', text_riesg).group()
                    clase_riesgo = re.sub('Comuna(:| )+','',clase_riesgo)
                    Clase_riesgo.append(clase_riesgo)

                    sub_clase = re.search('Localidad( |:)+([A-z]+|[ÁÉÍÓÚáéíúó]| )+', text_riesg).group()
                    sub_clase = re.sub('Localidad( |:)+','',sub_clase)
                    Sub_clase.append(sub_clase)


                    #lucro cesante por riesgo:
                    lucro =' - '.join(evaluacion_regla('PERJUICIO POR PARALIZACION ([A-Z]+| )+?([0-9]+\.?,?)+',text_riesg, flaggs= re.M))
                    if len(lucro) == 0 :
                        Lucro_Cesante.append('Lucro Cesante: No se encuentra')
                        Lucro_Cesante_log  = 'Lucro Cesante: No se encontra el patrón estandar definido para la familia.'
                    else:
                        Lucro_Cesante.append(lucro)
                        Lucro_Cesante_log = ''
                    #posicion  COBERTURAS
                    if pd.isna(re.search('COBERTURAS CONCEPTOS Monto Tasa Anual Prima', text_riesg, re.I)) == False:
                        ind_cobert=  re.finditer('COBERTURAS CONCEPTOS Monto Tasa Anual Prima', text_riesg,re.I)
                        ind_cobert_start= [m.end(0) for m in ind_cobert]
                        ind_cobert =  re.finditer('Total Item', text_riesg)
                        ind_cobert_end= [m.start(0) for m in ind_cobert]
                        #extraer texto
                        text_cobert.append(text_riesg[ind_cobert_start[0]:ind_cobert_end[0]])  
                    else:
                        text_cobert.append('No se encontro')
                    
        #Recompila la información de coberturas y lucro cesante     
        textos_coberturas = pd.DataFrame({'Numero':Num_Riesgo,'Dirr_riesgo':Dircc_riesgo,
                                      'Clase_riesg': Clase_riesgo, 'sub_Clase_riesgo': Sub_clase,
                                    'Texto':text_cobert})
        base_lucro = pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza]*len(Num_Riesgo),
                                'Riesgo_ID':Num_Riesgo, 
                                'Direccion_Riesgo':Dircc_riesgo, 
                                'Division_Administrativa_2':Clase_riesgo,
                                'Division_Administrativa_1':Sub_clase,
                                'Cobertura':Lucro_Cesante})  # se toma lucro cesante como cobertura.
        del Num_Riesgo, Dircc_riesgo, Clase_riesgo, Sub_clase
    
        for num in range(textos_coberturas.shape[0]):
            #Caracteristicas riesgo
            num_riesgo = textos_coberturas.Numero[num]
            dircc_riesgo= textos_coberturas.Dirr_riesgo[num]
            clase_riesgo= textos_coberturas.Clase_riesg[num]
            sub_clase= textos_coberturas.sub_Clase_riesgo[num]
            #coberturas 
            nombre_cober = evaluacion_regla('[0-9][0-9]+ [A-Z]( |[A-Z])+',textos_coberturas.Texto[num], re.M)
            Monto_Cober = evaluacion_regla('[A-Z]+\s*([0-9]+\s*\.?)+\s*,[0-9]+', textos_coberturas.Texto[num], re.M)
            monto_cober = [re.sub('[A-Z]+ ','',x) for x in Monto_Cober]
            #Compara longitudes
            if (len(nombre_cober) == len(Monto_Cober)) == False:
                longitudes = np.array([len(nombre_cober), len(Monto_Cober)])
                #toma el valor maximo
                long_max = max(longitudes)
                #Encuentra las posiciones menores
                posiciones = np.where(longitudes < long_max)
                posiciones = posiciones[0].tolist()[0]
                 #amplia su resultado.
                if posiciones[0] == 0:
                    nombre_cober = nombre_cober+ ['']*(long_max -len(nombre_cober))
                if posiciones[0] == 1:
                    Monto_Cober = Monto_Cober ['']*(long_max -len(Monto_Cober))
                    
                    
            if ii == 0 and num == 0:
                Base_cobert = pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza]*len(nombre_cober),
                                    'Riesgo_ID':[num_riesgo]*len(nombre_cober), 
                                    'Direccion_Riesgo':[dircc_riesgo]*len(nombre_cober),
                                    'Pais': [Pais]*len(nombre_cober),
                                     #Segementación de la ubicación : Trabajo Futuro.#
                                    'Descrip_Riesg_1':['']*len(nombre_cober), 'Descrip_Riesg_2':['']*len(nombre_cober) ,
                                    #-------------------------------------------------#
                                    'Division_Administrativa_1':[sub_clase]*len(nombre_cober),
                                    'Division_Administrativa_2': [clase_riesgo]*len(nombre_cober),
                                     #Segementación de la ubicación : Trabajo Futuro.
                                    'Codigo_Postal': ['']*len(nombre_cober), 'Latitud': ['']*len(nombre_cober), 'Longitud': ['']*len(nombre_cober),
                                    #-------------------------------------------------#
                                    'Cobertura':nombre_cober, 'Valor_Cobert': monto_cober,'Moneda_Valor_Cobertura':[Emitida]*len(nombre_cober),
                                    #Segementación de la ubicación : Trabajo Futuro.
                                    'Valor_Cobertura_%': ['']*len(nombre_cober), 'Base_Valor_Cobertura_%' : ['']*len(nombre_cober),'Valor_Cobertura_Lucro_cesante_(dias)': ['']*len(nombre_cober),
                                    'Prima_Cobertura': ['']*len(nombre_cober), 'Moneda_Prima_Cobertura': ['']*len(nombre_cober)
                                    #-------------------------------------------------#
                                    })     
            else:
                base_cobert =  pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza]*len(nombre_cober),
                                    'Riesgo_ID':[num_riesgo]*len(nombre_cober), 
                                    'Direccion_Riesgo':[dircc_riesgo]*len(nombre_cober),
                                    'Pais': [Pais]*len(nombre_cober),
                                     #Segementación de la ubicación : Trabajo Futuro.#
                                    'Descrip_Riesg_1':['']*len(nombre_cober), 'Descrip_Riesg_2':['']*len(nombre_cober) ,
                                    #-------------------------------------------------#
                                    'Division_Administrativa_1':[sub_clase]*len(nombre_cober),
                                    'Division_Administrativa_2': [clase_riesgo]*len(nombre_cober),
                                     #Segementación de la ubicación : Trabajo Futuro.
                                    'Codigo_Postal': ['']*len(nombre_cober), 'Latitud': ['']*len(nombre_cober), 'Longitud': ['']*len(nombre_cober),
                                    #-------------------------------------------------#
                                    'Cobertura':nombre_cober, 'Valor_Cobert': monto_cober,'Moneda_Valor_Cobertura':[Emitida]*len(nombre_cober),
                                    #Segementación de la ubicación : Trabajo Futuro.
                                    'Valor_Cobertura_%': ['']*len(nombre_cober), 'Base_Valor_Cobertura_%' : ['']*len(nombre_cober),'Valor_Cobertura_Lucro_cesante_(dias)': ['']*len(nombre_cober),
                                    'Prima_Cobertura': ['']*len(nombre_cober), 'Moneda_Prima_Cobertura': ['']*len(nombre_cober)
                                    #-------------------------------------------------#
                                    })
                
                Base_cobert = pd.concat([Base_cobert,base_cobert])
        
        #une las bases de datos
        Base_cobert = pd.concat([Base_cobert, base_lucro])
        Base_cobert = Base_cobert.drop_duplicates()
                
        #BASE DE LOGS -----------------------------#
        if ii == 0:
            Base_cobert_log = pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza],
                                    'Riesgo_ID': [num_riesgo_log],'Direccion_Riesgo': [dircc_riesgo_log],'Pais': [Pais_log],
                                     #Segementación de la ubicación : Trabajo Futuro.#
                                    'Descrip_Riesg_1':[''], 'Descrip_Riesg_2':[''],
                                    #-------------------------------------------------#
                                    'Division_Administrativa_1':[sub_clase_log],'Division_Administrativa_2': [clase_riesgo_log],
                                     #Segementación de la ubicación : Trabajo Futuro.
                                    'Codigo_Postal': [''], 'Latitud': [''], 'Longitud': [''],
                                    #-------------------------------------------------#
                                    'Cobertura': [nombre_cober_log], 'Valor_Cobert': [monto_cober_log],'Moneda_Valor_Cobertura':[Emitida_log],
                                    #Segementación de la ubicación : Trabajo Futuro.
                                    'Valor_Cobertura_%': [''], 'Base_Valor_Cobertura_%' : [''],'Valor_Cobertura_Lucro_cesante_(dias)': [''], 'Prima_Cobertura': [''], 'Moneda_Prima_Cobertura': [''],
                                    #-------------------------------------------------#
                                    #'Lucro_cesante_raw': [Lucro_Cesante_log]
                                    }) 
        if ii != 0:
            base_cobert_log = pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza],
                                    'Riesgo_ID': [num_riesgo_log],'Direccion_Riesgo': [dircc_riesgo_log],'Pais': [Pais_log],
                                     #Segementación de la ubicación : Trabajo Futuro.#
                                    'Descrip_Riesg_1':[''], 'Descrip_Riesg_2':[''],
                                    #-------------------------------------------------#
                                    'Division_Administrativa_1':[sub_clase_log],'Division_Administrativa_2': [clase_riesgo_log],
                                     #Segementación de la ubicación : Trabajo Futuro.
                                    'Codigo_Postal': [''], 'Latitud': [''], 'Longitud': [''],
                                    #-------------------------------------------------#
                                    'Cobertura': [nombre_cober_log], 'Valor_Cobert': [monto_cober_log],'Moneda_Valor_Cobertura':[Emitida_log],
                                    #Segementación de la ubicación : Trabajo Futuro.
                                    'Valor_Cobertura_%': [''], 'Base_Valor_Cobertura_%' : [''],'Valor_Cobertura_Lucro_cesante_(dias)': [''], 'Prima_Cobertura': [''], 'Moneda_Prima_Cobertura': [''],
                                    #-------------------------------------------------#
                                    #'Lucro_cesante_raw': [Lucro_Cesante_log]
                                    }) 
            Base_cobert_log = pd.concat([Base_cobert_log, base_cobert_log])
    return(Base_cobert, Base_cobert_log )

#FUNCION:Carac_Riesgos_Sura_Dos
#OBJETIVO: Resultados de la extraccipón de los valores: Deducible, coaseguradora,limites
#INTPUT: Matriz de polizas identificadas como de SURA.
#OUTPUT: 1. Matriz con las caracteristicas de valor asociadas a cada poliza (total asegurado, Total asegurable, Prima total)
#        2. Mariz de LOGS  con las caracteristicas de valor asociadas. 
def Caract_Generales_SURA_Dos(Documents_SURA):
    for ii in range(Documents_SURA.shape[0]):
        
        Nombre_Poliza = Documents_SURA.nombres_doc[ii]
        Aseguradora = Documents_SURA.Aseguradora[ii]
        texto = Documents_SURA.Textos[ii]
        
        #--------- Regla de deducible ---------#
        if pd.isna(re.search(' Deducibles ',texto)) == False:
            ind_deducible = re.search(' Deducibles ',texto).span()[0]
            end_deducible = re.search(' Perjuicio por Paralizaci[óo]n', texto[ind_deducible::]).span()[0]
            Deducible = texto[ind_deducible:end_deducible + ind_deducible]
            Deducible_log = ''
        else:
            Deducible = 'No se encontro'
            Deducible_log =  'No se encontra el patrón estandar definido para la familia.'
            
        ##--------- Regla de Limites/sublimites ---------#
        if pd.isna(re.search(' Sub-L[ií]mites( )+(\(Aplican en |[A-Z]|se aplican)?',texto)) == False:
            pst = re.finditer(' Sub-L[ií]mites( )+(\(Aplican en |[A-Z]|se aplican)?',texto)
            Posicion = [m.start()  for m in pst]
            if len(Posicion) == 1:
                ind_limite = Posicion[0]
            else:
                ind_limite = Posicion[len(Posicion)-1]
            if pd.isna( re.search(' (Nota|Ingl[ée]s|Cl[aá]usula|Deducibles)', texto[ind_limite::])) == False:
                end_limite = re.search(' (Nota|Ingl[ée]s|Cl[aá]usula|Deducibles)', texto[ind_limite::]).span()[0]
            else: 
                end_limite = 300
            Limites = texto[ind_limite:ind_limite +end_limite]
            Limites_log = ''
        else:
            Limites = 'No se encontro'
            Limites_log =  'No se encontra el patrón estandar definido para la familia.'
        
       #--------- Regla de Participacion/Coaseguradora ---------#
        if pd.isna(re.search('son asumidos en coaseguro por las siguientes', texto)) == False:
            ind_coasegura = re.search('son asumidos en coaseguro por las siguientes( |[A-z])+', texto).span()[1]
            end_coasegura = re.search('LIDER', texto[ind_coasegura::]).span()[0]
            coaseguradora = texto[ind_coasegura:ind_coasegura+end_coasegura]
            coaseguradora = re.search(' ([0-9]+\,?\.?)+ ', coaseguradora).group()
            coaseguradora = re.sub(' ','',coaseguradora)
            coaseguradora_log = ''
        else:
            coaseguradora = 'No se encontro'
            coaseguradora_log =  'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Cohstruccion de matrices ---------#
        if ii == 0:
            Base_General = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza],   
                                         'Deducibles_raw' : [Deducible],  
                                          #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                          #----------------#
                                         'Limites_raw': [Limites],'Participacion': [coaseguradora] })
            Base_General_log = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza],   
                                         'Deducibles_raw' : [Deducible_log],  
                                          #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                          #----------------#
                                         'Limites_raw': [Limites_log],'Participacion': [coaseguradora_log] })
            
            
        else: 
            base_general = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza],   
                                         'Deducibles_raw' : [Deducible],  
                                          #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                          #----------------#
                                         'Limites_raw': [Limites],'Participacion': [coaseguradora] })
            base_general_log = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza],   
                                         'Deducibles_raw' : [Deducible_log],  
                                          #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                          #----------------#
                                         'Limites_raw': [Limites_log],'Participacion': [coaseguradora_log] })
        
            Base_General = pd.concat([ Base_General , base_general])
            Base_General_log = pd.concat([ Base_General_log , base_general_log ])
            
            
    return(Base_General  , Base_General_log)

#FUNCION: Carac_Riesgos_Sura
#OBJETIVO: Resultados de la extraccipón de los valores: Total asegurado, Total aseguralbe,Prima total
#INTPUT: Matriz de polizas identificadas como de SURA.
#OUTPUT: 1. Matriz con las caracteristicas de valor asociadas a cada poliza (total asegurado, Total asegurable, Prima total)
#        2. Matriz de LOGS de las caracteristicas de valor asociadas.
def Caract_Generales_SURA(Documents_SURA):
    for ii in range(Documents_SURA.shape[0]):
        texto = Documents_SURA.Textos[ii]
        Nombre_Poliza = Documents_SURA.nombres_doc[ii]
    
        #--------- Regla de Total asegurado ---------#
        if pd.isna(re.search(r'L[íi]mite(de| )+Indemni(\w+|ó)+( |\(|\)|[A-z]+|[áéíóúñÁÉÍÓÚÑ]|\$)+([0-9]+|\.)+',texto)) == False:
            total_asegurado = re.search(r'L[íi]mite(de| )+Indemni(\w+|ó)+( |\(|\)|[A-z]+|[áéíóúñÁÉÍÓÚÑ]|\$)+([0-9]+|\.)+',texto).group()
            total_asegurado = re.search('( [A-Z]{2,}|\$| )+?([0-9]+|\.)+', total_asegurado).group()
            total_asegurado = total_asegurado.split()
            if len(total_asegurado) == 2:
                Emitida_Asegurado = total_asegurado[0]
                Total_Asegurado = re.sub('(\.)+','.', total_asegurado[1])
                Total_Asegurado_log = ''
                Emitida_Asegurado_log = ''
            if len(total_asegurado) == 1:
                if pd.isna(re.search('[0-9]+\.',total_asegurado[0])) == False:
                    Total_Asegurado = total_asegurado[0]
                    Emitida_Asegurado = 'No se encontro'
                    Total_Asegurado_log = ''
                    Emitida_Asegurado_log = 'No se encontra el patrón estandar definido para la familia.'
                else:
                    Total_Asegurado = 'No se encontro'
                    Emitida_Asegurado =  total_asegurado[0]
                    Total_Asegurado_log = 'No se encontra el patrón estandar definido para la familia.'
                    Emitida_Asegurado_log = ''
        else:
            Emitida_Asegurado = 'No se encontro'
            Total_Asegurado = '0'
            Emitida_Asegurado_log = 'No se encontra el patrón estandar definido para la familia.'
            Total_Asegurado_log = 'No se encontra el patrón estandar definido para la familia.'
            
        #--------- Regla de Total asegurable ---------#
        if pd.isna(re.search(r'SUMA ASEGURABLE',texto)) == False:
            Total_Asegurable.append(re.search(r'(?<= SUMA ASEGURABLE)( )+?[A-Z]+ ([0-9]+|\.)+',texto).group())
            Error_Total_asegurable = ''
            Emitida_Asegurable = ''
            Emitida_Asegurable_log = ''
        else:
            Emitida_Asegurable = Emitida_Asegurado
            Total_Asegurable = Total_Asegurado 
            Total_Asegurable_log = 'Se asume igual a Total Asegurado.'  
            Emitida_Asegurable_log = 'Se asume igual a Moneda Emitida Asegurado.'
            
        #--------- Regla de Prima total---------#
        if pd.isna(re.search('(?<= PRIMA TOTAL)( |:)+([0-9]+|\.|,)+', texto)) == False:
            prima_total = re.search('(?<= PRIMA TOTAL)( |:)+([0-9]+|\.|,)+', texto).group()
            Prima_Total = re.sub(':','', prima_total)
            Prima_Total_log = ''
        else:
            Prima_Total = ''
            Prima_Total_log = 'No se encuentra el patrón estandar definido para la familia.'
            
        #--------- Construccion de matrices ---------#
        if ii ==  0:
            Base_Genral = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 
                            'Total_Asegurado': [Total_Asegurado],'Moneda_Total_Asegurado':[Emitida_Asegurado], 
                            'Total_Asegurable': [Total_Asegurable], 'Moneda_Total_Asegurable':[Emitida_Asegurado],
                            'Total_Prima': [Prima_Total], 'Moneda_Prima':[Emitida_Asegurado]
                           })
            Base_Genral_log = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 
                            'Total_Asegurado': [Total_Asegurado_log],'Moneda_Total_Asegurado':[Emitida_Asegurado_log], 
                            'Total_Asegurable': [Total_Asegurable_log], 'Moneda_Total_Asegurable':[Emitida_Asegurado_log],
                            'Total_Prima': [Prima_Total_log], 'Moneda_Prima':[Emitida_Asegurado_log]  })
        else:
            base = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 
                            'Total_Asegurado': [Total_Asegurado],'Moneda_Total_Asegurado':[Emitida_Asegurado], 
                            'Total_Asegurable': [Total_Asegurable], 'Moneda_Total_Asegurable':[Emitida_Asegurado],
                            'Total_Prima': [Prima_Total], 'Moneda_Prima':[Emitida_Asegurado]
                           })
            base_log =  pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 
                            'Total_Asegurado': [Total_Asegurado_log],'Moneda_Total_Asegurado':[Emitida_Asegurado_log], 
                            'Total_Asegurable': [Total_Asegurable_log], 'Moneda_Total_Asegurable':[Emitida_Asegurado_log],
                            'Total_Prima': [Prima_Total_log], 'Moneda_Prima':[Emitida_Asegurado_log]  })
            Base_Genral = pd.concat([Base_Genral, base])
            Base_Genral_log = pd.concat([Base_Genral_log, base_log])
            
    return(Base_Genral, Base_Genral_log)


#----- 2.3 FAMILIA PROVINCIA---------------------#
#FUNCION: Caracteristicas Basicas Provincia
#OBJETIVO: Extraer las caracterisiticas principales de la poliza (num poliza, certificado, tipo, etc)
#INPUT:  Matriz de polizas identificadas como de PROVINCIA.
#OUPUT: : 1. Matriz con las caracteristicas de valor asociadas a cada poliza.
#         2. Matriz de LOGS de las caracteristicas de valor asociadas.
def Carct_Basicas_PROVINCIA(Documents_PROVINCIA):
    for ii in range(Documents_PROVINCIA.shape[0]):
        texto = Documents_PROVINCIA.Textos[ii]
        Ubic_pol= Documents_PROVINCIA.nombres_doc[ii]
        Aseguradora = Documents_PROVINCIA.Aseguradora[ii]
        #--------- Regla de Numero de poliza ---------#
        if pd.isna(re.search(r'([0-9]|\.|\-)+ Certificado',texto)) == False:
            Nm_poliza = re.search(r'([0-9]|\.|\-)+ Certificado',texto).group()
            Num_poliza  = re.sub('Certificado','',Nm_poliza)
            Num_poliza_log =  ''
        else:
            Num_poliza = ''
            Num_poliza_log =  'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de Certificado---------#
        if pd.isna(re.search('Certificado Domicilio Telefono [0-9]+', texto)) == False:
            Certificado = evaluacion_regla('Certificado Domicilio Telefono [0-9]+', texto, re.M|re.I)[0]
            Certificado = re.sub('Certificado Domicilio Telefono( )+','',Certificado)
            Certificado_log = ''
        else:
            Certificado = ''
            Certificado_log =  'No se encuentra el patrón estandar definido para la familia.'
            
        #--------- Regla de Tipo de poliza---------#
        #Para "Tipo de Poliza" se toma el primer conjunto de palabras que anteceden a "Asegurado" #
        tipo = ''
        if pd.isna( re.search(r'([A-Z]+| ){2,4} Asegurado',texto)) == False:
            tipo = re.search(r'([A-Z]+| ){2,4} Asegurado',texto).group()
            Tipo = re.sub('Asegurado','', tipo)
            Tipo_log = ''
        else:
            Tipo = ''
            Tipo_log = 'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de Fechas de vigencia---------#
        Fecha_vigencia_Desde = 'No se encontro'
        Fecha_vigencia_Hasta = 'No se encontro'
        Fecha_vigencia_Desde_log = 'No se encuentra el patrón estandar definido para la familia.'
        Fecha_vigencia_Hasta_log = 'No se encuentra el patrón estandar definido para la familia.'
        if (pd.isna(re.search('Desde las 12 hs(\.)?', texto))== pd.isna(re.search('Hasta las 12 hs(\.)? ([0-9]+\/)+[0-9]+', texto)) == False) == True:
            text_fecha = texto[re.search('Desde las 12 hs(\.)?', texto).span()[0]:re.search('Hasta las 12 hs(\.)? ([0-9]+\/)+[0-9]+', texto).span()[1]]
            if(len(text_fecha)  > 0):
                fecha_vigencia = evaluacion_regla( r'([0-9]+\/)+[0-9]+',text_fecha, re.M)
                Fecha_vigencia_Desde = fecha_vigencia[0]
                Fecha_vigencia_Hasta = fecha_vigencia[1]
                Fecha_vigencia_Desde_log = ''
                Fecha_vigencia_Hasta_log = ''
            
        #--------- Regla de Tomador ---------#
        if pd.isna(re.search('([A-Z]|[ÁÉÍÓÚÑ]|,|\$|\.|[0-9]| )+TIPO DE SEGURO Tomador', texto)) == False:
            tomador = re.search('([A-Z]|[ÁÉÍÓÚÑ]|,|\$|\.|[0-9]| )+TIPO DE SEGURO Tomador', texto).group()
            num_tomador = re.search('([0-9][A-Z]?(\.)?(,)?)+',tomador).group()
            Tomador = re.sub(num_tomador + '.+','', tomador)
            Tomador_log = ''
        else:
            Tomador = ''
            Tomador_log = 'No se encuentra el patrón estandar definido para la familia.'
            
        #--------- Regla de ID Tomador ---------#
        if pd.isna(re.search('(?<=Localidad y Provincia)([A-z]+| )*([0-9]+-?)*',texto)) == False:
            ID_Tomador = re.search('(?<=Localidad y Provincia)([A-z]+| )*([0-9]+-?)*',texto).group()
            ID_Tomador = re.sub('([A-z]+| )+','',ID_Tomador)
            ID_Tomador_log = ''
        else:
            ID_Tomador = ''
            ID_Tomador_log = 'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de Asegurado ---------#
        if pd.isna(re.search('([A-Z]|[ÁÉÍÓÚÑ]|,|\$|\.|[0-9]| )+Asegurado', texto)) == False:
            asegurado = re.search('([A-Z]|[ÁÉÍÓÚÑ]|,|\$|\.|[0-9]| )+Asegurado', texto).group()
            asegurado = re.sub('Asegurado','',asegurado)
            Asegurado = re.sub(tipo,'',asegurado)
            Asegurado_log = ''
        else:
            Asegurado = ''
            Asegurado_log = 'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de ID Asegurado ---------#
        #En la muestra predefinida no se encuentra un patron para ID Asegurado #
        ID_Asegurado = ''
        ID_Asegurado_log = 'No existe una regla para esta familia.'
        
        #--------- Regla de tipo de moneda Emitida ---------#
        if pd.isna(re.search('(?<=Moneda)( |:)+([A-Z]+| ){2,4}', texto)) == False:
            emitida = re.search('(?<=Moneda)( |:)+([A-Z]+| ){2,4}', texto).group()
            Emitida = re.sub(': ','', emitida)
            Emitida = re.sub(' EL','',Emitida)
            Emitida_log = ''
        else:
            Emitida = ''
            Emitida_log = 'No se encuentra el patrón estandar definido para la familia.'
            
        #--------- Construcción de las bases  ---------#
        if ii == 0:
            Base_Inicio_PROVINCIA = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta],
                                                   'Moneda_Poliza':[Emitida],
                                                   'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                                  'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]}) 
            Base_Inicio_PROVINCIA_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log],
                                                   'Moneda_Poliza':[Emitida_log],'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                                  'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]}) 
    
    
        else:
            base_inico_prov = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta],
                                                   'Moneda_Poliza':[Emitida],
                                                   'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                                  'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]}) 
            base_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                                  'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                                  'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log],
                                                   'Moneda_Poliza':[Emitida_log],'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                                  'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]}) 
          
            Base_Inicio_PROVINCIA = pd.concat([Base_Inicio_PROVINCIA, base_inico_prov])
            Base_Inicio_PROVINCIA_log = pd.concat([Base_Inicio_PROVINCIA_log ,base_log ])
    return(Base_Inicio_PROVINCIA, Base_Inicio_PROVINCIA_log  )
#FUNCION: Caracteristicas de Riesgos y Coberturas PROVINCIA
#OBJETIVO: Identificar los riesgos con sus respectivas coberturas.
#INPUT: Matriz de polizas identificadas como de Provincia.
#OUPUT: 1. Matriz con riesgos y coberturas asociadas a cada una de las polizas.
#       2. Matriz de LOGS de cada uno de los valores asociados.
def Caract_Riesgos_Cobertura_PROVINCIA(Documents_PROVINCIA):
    for ii in range(Documents_PROVINCIA.shape[0]):
        texto = Documents_PROVINCIA.Textos[ii]
        Nombre_POLIZA = Documents_PROVINCIA.nombres_doc[ii]
    
        #--------- Regla de Pais del Riesgo ---------#
        regla_pais = ' (C[Oo][Ll][Oo][Mm]Bb][Ii][Aa]|P[eE][rR][uúUÚ]|E[cC][uU][aA][dD][oO][rR]|A[rR][gG][eE][nN][tT][iI][nN][aA]|B[oO][lL][iI][vV][iI][aA]|B[rR][aA][sS][iI][lL]|C[hH][iI][lL][eE]|C[uU][bB][aA]|E[lL] S[aA][lL][vV][aA][dD][oO][rR]|G[uU][aA][tT][eE][Mm][aA][lL][aA]|H[oO][nN][dD][uU][rR][aA][sS]|M[ÉEeé][xX][iI][Cc][oO]|N[iI][cC][aA][rR][aA][gG][uU][aA]|P[aA][nN][aA][mM][aáAÁ]|P[aA][rR][aA][gG][uU][aA][yY]|P[uU][eE][rR][tT][oO] R[iI][cC][oO]|U[rR][uU][gG][uU][aA][yY]|V[eE][nN][eE][zZ][uU][eE][lL][aA])'
        
        if pd.isna(re.search(regla_pais,texto)) == False:
            value_pais = [x.strip(' ') for x in evaluacion_regla(regla_pais, texto, re.I|re.M)]
            value_pais = collections.Counter(value_pais)
            Pais = value_pais.most_common()[0][0]
            Pais_log =''
        else:
            Pais = 'No determinado'
            Pais_log = 'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de Riesgos, Coberturas, lucro cesantes ---------#
        #inicializa variables:
        DIRC_RIESGO = 'No se encontro'
        DIRC_RIESGO_log = 'No se encuentra aplicando la regla establecida.'
        UBICACION= 'No se encontro'
        UBICACION_log = 'No se encuentra aplicando la regla establecida.'
        CLASE_RIESGO= 'No se encontro'
        CLASE_RIESGO_log= 'No se encuentra aplicando la regla establecida.'
        #Encuentra la posicion de los posibles Riesgos:
        ind_riesgo = re.finditer(' RIESGOS ASEGURADOS', texto)
        ind_riesgo_start= [m.start(0) for m in ind_riesgo]
        flat_list = unique(ind_riesgo_start)
        #Encuentra la posicion de las posibles coberturas:
        ind_cobert =  re.finditer(' Capital Total:', texto)
        ind_cobert_end= [m.start(0) for m in ind_cobert]
        
        #Extrae el texto entre la posicion del riesgo y la cobertura.
        if len(ind_riesgo_start) > 0:
            Texto_Riesgo = []
            for num in range(len(ind_cobert_end)):
                Texto_Riesgo.append(texto[flat_list[num]:ind_cobert_end[num]])
            
            #Unifica los textos de riesgo.
            Texto_Riesgo = unique(Texto_Riesgo)
            
            #Por cada texto de riesgo se extrae el Riesgo, Cobertura y Monto :
            for txt_rieg  in  range(len(Texto_Riesgo)):
                text_cob = Texto_Riesgo[txt_rieg]
                ind_riesg_cob = re.finditer('Capital %Franquicia Monto Fcia.', text_cob)
                ind_cobert_star = [m.start(0) for m in ind_riesg_cob]
                nombre_cober = ['No se encontro'] 
                if len(ind_cobert_star) > 0:
                    Texto_Cobert= text_cob[ind_cobert_star[0]:]
                    nombre_cobertura =  evaluacion_regla('[A-Z]{4,}([A-Z]| |-|,|\(|\))+ ([0-9]{3,})', Texto_Cobert, re.M)
                    if len(nombre_cobertura) > 0:
                        nombre_cober = nombre_cobertura
                    if len(nombre_cobertura) == 0:
                        ind_cobert_new = re.search(' Coberturas [A-Z]', Texto_Cobert)
                        if pd.isna(ind_cobert_new) == False:
                            ind_cobert_new = ind_cobert_new.span()[1]-1
                            nombre_cobertura = evaluacion_regla('[A-Z]{4,}([A-Z]| |-|,|\(|\))+ ([0-9]{3,})?',Texto_Cobert[ind_cobert_new:], re.M)
                            if len(nombre_cobertura) > 0:
                                nombre_cober = nombre_cobertura
                                
                #Encuentra la ubicacion del riesgo: 
                #Inicializa la variable
                UBICACION = 'No se encontro' 
                UBICACION_log = 'No se encontro aplicando la primera regla.'
                Ubicacion_riesg = re.search('(Ubicaci[óo]n Ri(esgo)?|AMBITO DE LA COBERTURA):( )+?([A-Z]+|\s+|[0-9]|\.|//|\(|\))+', text_cob)
                
                #Aplica la regla
                if pd.isna(re.search('(Ubicaci[óo]n Ri(esgo)?|AMBITO DE LA COBERTURA):( )+?([A-Z]+|\s+|[0-9]|\.|//|\(|\))+', text_cob) ) == False:
                    UBICACION = re.search('(Ubicaci[óo]n Ri(esgo)?|AMBITO DE LA COBERTURA):( )+?([A-Z]+|\s+|[0-9]|\.|//|\(|\))+', text_cob).group()
                    UBICACION = re.sub('(Ubicaci[óo]n Riesgo|AMBITO DE LA COBERTURA):( )+?','',UBICACION)
                    UBICACION_log = ''
                else:
                    if pd.isna(re.search('(Ubicaci[óo]n Ri(esgo)?|AMBITO DE LA COBERTURA):?', text_cob)) == False:
                        ind_ub_start = re.search('(Ubicaci[óo]n Ri(esgo)?|AMBITO DE LA COBERTURA):?', text_cob).span()[1]
                        ind_ub_end =  re.search(':', text_cob[ind_ub_start:]).span()[0]
                        UBICACION = text_cob[ind_ub_start:ind_ub_start+ind_ub_end]
                        UBICACION_log = ''
                
                #Inicializa la variable
                DIRC_RIESGO = 'No se encontro'
                DIRC_RIESGO_log = 'No se encontro aplicando la primera regla.'
                #Aplica la regla
                if pd.isna(re.search('Riesgo:( )+?([A-Z]+|\s+|[0-9]|\.)+', text_cob)) == False:
                    DIRC_RIESGO = re.search('Riesgo:( )+?([A-Z]+|\s+|[0-9]|\.)+', text_cob).group()
                    DIRC_RIESGO = re.sub('Riesgo:( )+?','',DIRC_RIESGO)
                    DIRC_RIESGO_log =  ''
                
                #Inicializa la variable
                CLASE_RIESGO = 'No se encontro'
                CLASE_RIESGO_log = ''
                #Aplica la regla
                if pd.isna(re.search('Plan:( )+?([A-Z]+| |\.)+', text_cob)) == False:
                    CLASE_RIESGO = re.search('Plan:( )+?([A-Z]+| |\.)+', text_cob).group()
                    CLASE_RIESGO = re.sub('Plan:( )+','',CLASE_RIESGO)
                    CLASE_RIESGO_log = ''

                for num_t in range(len(nombre_cober)):
                    #Inicializa la variable
                    Monto_Cober = 'No se encontro'
                    Monto_nombre = 'No se encontro'
                    #Aplica la regla
                    if  nombre_cober[num_t] != 'No se encontro':
                        if pd.isna(re.search('[A-z]+ [0-9]+' , nombre_cober[num_t])) == False:
                            Monto_Cober = re.search('[0-9]+' , nombre_cober[num_t]).group()
                            Monto_nombre =  re.search('([A-Z]( )?|-)+', nombre_cober[num_t]).group()
                        else:
                            Monto_nombre = nombre_cober[num_t] 
                       
                    
                    if ii == 0 and num_t == 0 and txt_rieg == 0:
                        Base_Cobert_Provincia = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 
                                         'Riesgo_ID': [DIRC_RIESGO] ,
                                        'Pais' :Pais,
                                        'Direccion_Riesgo':[UBICACION] ,
                                        'Descrip_Riesg_1':[CLASE_RIESGO],
                                          #Segementación de la ubicación : Trabajo Futuro.
                                        'Descrip_Riesg_2':'',
                                        'Division_Administrativa_1':'', 'Division_Administrativa_2': '','Division_Administrativa_3':'',
                                        'Codigo_Postal': '', 'Latitud':'', 'Longitud': '',
                                        #--------------------------------------------------#
                                        'Cobertura':[Monto_nombre], 'Valor_Cobert': [Monto_Cober],
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%': '', 'Base_Valor_Cobertura_%' : '','Valor_Cobertura_Lucro_cesante_(dias)': '',
                                        'Prima_Cobertura': '', 'Moneda_Prima_Cobertura': ''
                                        #--------------------------------------------------#  
                                        }) 
                    else:
                        base_cobert =  pd.DataFrame({'Nombre_Poliza':Nombre_POLIZA, 
                                             'Riesgo_ID': [DIRC_RIESGO],
                                            'Pais':Pais,
                                            'Direccion_Riesgo':[UBICACION],
                                            'Descrip_Riesg_1':[CLASE_RIESGO], 
                                            #Segmentación de la ubicación : Trabajo Futuro.
                                            'Descrip_Riesg_2':'', 
                                            'Division_Administrativa_1':'','Division_Administrativa_2': '','Division_Administrativa_3':'',
                                            'Codigo_Postal': '', 'Latitud':'', 'Longitud': '',
                                            #--------------------------------------------------#
                                            'Cobertura':[Monto_nombre], 'Valor_Cobert': [Monto_Cober],
                                            #Segementación de la ubicación : Trabajo Futuro.
                                            'Valor_Cobertura_%': '', 'Base_Valor_Cobertura_%' : '','Valor_Cobertura_Lucro_cesante_(dias)': '',
                                            'Prima_Cobertura': '', 'Moneda_Prima_Cobertura': ''
                                            #--------------------------------------------------#         
                                            })
                        base_cobert = base_cobert.drop_duplicates()
                        #concatena las bases de coberturas
                        Base_Cobert_Provincia = pd.concat([Base_Cobert_Provincia,base_cobert])
                        
        #--------- Construye la matriz de logs ---------#
        if ii == 0:
            Base_Cobert_Provin_log = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 
                                        'Riesgo_ID': [DIRC_RIESGO_log ] , 'Pais': [Pais_log],
                                        'Direccion_Riesgo':[UBICACION_log ] ,'Descrip_Riesg_1':[CLASE_RIESGO_log],
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Descrip_Riesg_2':'', 
                                        'Division_Administrativa_1':'','Division_Administrativa_2': '','Division_Administrativa_3':'',
                                        'Codigo_Postal': '', 'Latitud':'', 'Longitud': '',
                                        #--------------------------------------------------#
                                        'Cobertura':[''], 'Valor_Cobert': [''],
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%': '', 'Base_Valor_Cobertura_%' : '','Valor_Cobertura_Lucro_cesante_(dias)': '',
                                        'Prima_Cobertura': '', 'Moneda_Prima_Cobertura': ''
                                        #--------------------------------------------------#  
                                        })
        else:
            base_log = pd.DataFrame({'Nombre_Poliza':[Nombre_POLIZA], 'Riesgo_ID': [DIRC_RIESGO_log ] , 'Pais': [Pais_log],
                                        'Direccion_Riesgo':[UBICACION_log ] ,'Descrip_Riesg_1':[CLASE_RIESGO_log],
                                        #Segmentación de la ubicación : Trabajo Futuro.
                                        'Descrip_Riesg_2':'',
                                        'Division_Administrativa_1':'','Division_Administrativa_2': '','Division_Administrativa_3':'',
                                        'Codigo_Postal': '', 'Latitud':'', 'Longitud': '',
                                        #--------------------------------------------------#
                                        'Cobertura':[''], 'Valor_Cobert': [''],
                                        #Segementación de la ubicación : Trabajo Futuro.
                                        'Valor_Cobertura_%': '', 'Base_Valor_Cobertura_%' : '','Valor_Cobertura_Lucro_cesante_(dias)': '',
                                        'Prima_Cobertura': '', 'Moneda_Prima_Cobertura': ''
                                        #--------------------------------------------------#  
                                        })
            #concatena la base de log(error)
            Base_Cobert_Provin_log = pd.concat([ Base_Cobert_Provin_log , base_log ])
    return(Base_Cobert_Provincia , Base_Cobert_Provin_log)
    
#FUNCION: Caracteristicas Generales PROVINCIA
#OBJETIVO: Identificar y extraer los valosres totales que son generales en las polizas de PROVINCIA
#INPUT: Matriz de polizas identificadas como de Provincia.
#OUPUT: 1. Matriz caracteristicas de totales de cada una de las polizas.
#       2. Matriz de LOGS de cada uno de los valores asociados.
def Caract_Generales_PROVINCIA(Documents_PROVINCIA):
    
    for ii in range(Documents_PROVINCIA.shape[0]):
        
        Nombre_Poliza = Documents_PROVINCIA.nombres_doc[ii]
        texto = Documents_PROVINCIA.Textos[ii]
        
        #--------- Regla de Total Asegurado---------#
        if pd.isna(re.search('(?<=Capital Total)( |:)+([0-9]+\.?)+', texto)) == False:
            Total_Asegurado = re.search('(?<=Capital Total)( |:)+([0-9]+\.?)+', texto).group()
            Total_Asegurado = re.sub(':','',Total_Asegurado)
            Total_Asegurado_log = ''
        else:
            Total_Asegurado = ''
            Total_Asegurado_log = 'No se encuentra el patrón estandar definido para la familia.'
    
        #--------- Regla de Total Asegurable---------#
        #En la muestr actual no se escuentra una especificación del total asegurable#
        #Por lo cual se asume igual al total asegurado.#
        Total_Asegurable = Total_Asegurado
        Total_Asegurable_log = 'Se asume igual al Total Asegurado.'
        
        #--------- Regla de Total Prima ---------#
        if pd.isna(re.search('(?<=Premio)( )+?([0-9]+\.?)+',texto)) ==False:
            Prima_Total = re.search('(?<=Premio)( )+?([0-9]+\.?)+',texto).group()
            Prima_Total_log = ''
        else:
            Prima_Total = ''
            Prima_Total_log =  'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de Tipo Moneda Emitida ---------#
        if pd.isna(re.search('(?<=Moneda)( |:)+([A-Z]+| ){2,4}', texto)) == False:
            emitida = re.search('(?<=Moneda)( |:)+([A-Z]+| ){2,4}', texto).group()
            Emitida = re.sub(': ','', emitida)
            Emitida = re.sub(' EL','',Emitida)
            Emitida_log = ''
        else:
            Emitida = ''
            Emitida_log =  'No se encuentra el patrón estandar definido para la familia.'
            
        #--------- Regla de Deducibles--------#    
        if pd.isna(re.search('[0-9]+( |\))\s*FRANQUICIA DEDUCIBLE',texto)) == False :
            int_dedu = re.search('[0-9]+\)\s*FRANQUICIA DEDUCIBLE',texto).span()
            out_dedu = re.search('[0-9]+\)\s*[A-Z]+',texto[int_dedu[1]:]).span()[0]
            Deducible = texto[int_dedu[0]:int_dedu[1] + out_dedu]
            Deducible_log = ''
        else:
            Deducible =  'No se encontro'
            Deducible_log = 'No se encuentra el patrón estandar definido para la familia.'
        
        #--------- Regla de Limites ---------#
        #En la muestra actual no se encuentran registros de limites#
        #Por lo cual no se construyo una regla#
        Limites = 'No se encontro'
        Limites_log = 'No se realizó la regla para este item. '
        
        #--------- Regla de Lucro Cesante ---------#
        #Lucro cesante   se toma como cobertura                                       
        Lucro_Cesante =  'Lucro cesante: No se encontro'
        Lucro_Cesante_log = 'Lucro cesante: No se realizó la regla para este item. '
        
        #--------- Regla de Participación ---------#
        Participacion =  'No se encontro'
        Participacion_log =  'No se encuentra el patrón estandar definido para la familia.'
        if pd.isna(re.search('Se deja constancia que esta p[oó]liza se emite en [cC]oaseguro',texto, re.I)) == False:
            int_lim = re.search('Se deja constancia que esta p[oó]liza se emite en [cC]oaseguro',texto, re.I).span()
            out_lim = re.search('Piloto',texto[int_lim[1]:]).span()[1]
            Partipacion = texto[int_lim[0] : int_lim[1] + out_lim]
            Participacion = re.search('([0-9]+\.?)+%',Partipacion).group()
            Participacion_log = ''            
            
        #contrunccion de la base
        if ii == 0:
            Base_Genral = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado],
                            'Total_Asegurable': [Total_Asegurable], 'Total_Prima': [Prima_Total],
                            'Moneda_Prima':[Emitida], 'Moneda_Total_Asegurado':[Emitida], 'Moneda_Total_Asegurable':[Emitida],
                            'Deducibles_raw' : [Deducible],  
                            #Segementación de la ubicación : Trabajo Futuro.
                            'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                            'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                            #----------------#
                            'Cobertura': [Lucro_Cesante],
                            'Limites_raw': [Limites], 'Participacion': [Participacion] })
            
            Base_Genral_log = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado_log],
                            'Total_Asegurable': [Total_Asegurable_log], 'Total_Prima': [Prima_Total_log],
                            'Moneda_Prima':[Emitida_log], 'Moneda_Total_Asegurado':[Emitida_log],  'Moneda_Total_Asegurable':[Emitida_log],
                            'Deducibles_raw' : [Deducible_log],  
                            #Segementación de la ubicación : Trabajo Futuro.
                            'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                            'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                            #----------------#
                            'Cobertura': [Lucro_Cesante_log],
                            'Limites_raw': [Limites_log], 'Participacion': [Participacion_log] })
            
        else:
            base_genral =  pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado],
                            'Total_Asegurable': [Total_Asegurable], 'Total_Prima': [Prima_Total],
                            'Moneda_Prima':[Emitida], 'Moneda_Total_Asegurado':[Emitida],'Moneda_Total_Asegurable':[Emitida],
                            'Deducibles_raw' : [Deducible],  
                            #Segementación de la ubicación : Trabajo Futuro.
                            'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                            'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                            #----------------#
                            'Cobertura': [Lucro_Cesante],
                            'Limites_raw': [Limites], 'Participacion': [Participacion] })
            
            base_general_log = pd.DataFrame({'Nombre_Poliza': [Nombre_Poliza], 'Total_Asegurado': [Total_Asegurado_log],
                            'Total_Asegurable': [Total_Asegurable_log], 'Total_Prima': [Prima_Total_log],
                            'Moneda_Prima':[Emitida_log], 'Moneda_Total_Asegurado':[Emitida_log], 'Moneda_Total_Asegurable':[Emitida_log],
                            'Deducibles_raw' : [Deducible_log],  
                            #Segementación de la ubicación : Trabajo Futuro.
                            'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                            'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                            #----------------#
                            'Cobertura': [Lucro_Cesante_log],
                            'Limites_raw': [Limites_log], 'Participacion': [Participacion_log] })
            Base_Genral = pd.concat([Base_Genral, base_genral])
            Base_Genral_log = pd.concat([Base_Genral_log, base_general_log])
    return(Base_Genral,  Base_Genral_log )
    
    

#----- 2.4 FAMILIA GENERAL/OTROS---------------------#
#FUNCION: Reglas de otra familia
#OBJETIVO: Construir una matriz que contenga las reglas básicas generales de los campos principales.
#INPUT: Ninguno
#OUPUT: Matriz de reglas básicas para aplicar en Polizas que no pertenecen a ninguna familia.
def Regla_Otra_Familia():
    #----------------reglas generales----------------#
    regla_poliza = r'[Pp][oóOÓ][Ll][Ii][Zz][Aa](:|[Nn]|[Rr]|°|[0Oo]|\\.| )+?([A-z]+(-))?[0-9]{4,}'
    regla_asegurado =  r'( (DATOS DEL )?A[Ss][Ee][Gg][Uu][Rr][Aa][Dd][Oo]( NOMBRE|PRINCIPAL)?|ASEGURA A)(:| )+?(([A-Z]([a-zA-Z]+|[ÁÉÍÓÚáéíóíÑñ~\\\\.])+| )+|[of|[0-9]+|\\\\/])+'
    regla_Asegurado_o_tomador = 'A[Ss][Ee][Gg][Uu][Rr][Aa][Dd][oO]( |/|[YyOo])+T[Oo][Mm][Aa][Dd][Oo][Rr]'
    regla_tomador = r'([Tt][Oo][Mm][aA][dD][oO][rR]|(DATOS DEL )?[Cc][Oo][Nn][Tt][Rr][Aa][Tt][Aa][Nn][Tt][Ee]( NOMBRE)?|P[Rr][Oo][Pp][Oo][Nn][Ee][Nn][Tt][Ee])(:| )+?[A-Z]([A-z]|[ÁÉÍÓÚÑáéíóúñ])+( |[A-Z]([A-z]|[ÁÉÍÓÚÑáéíóúñ])+|\.|[0-9]|-|,|&|@|//|\(|\)|!|;|\+|\*)+'
    regla_beneficiario =r'(?<=B[Ee][Nn][Ee][Ff][Ii][Cc][Ii][Aa][Rr][Ii][Oo])(:| )+?(([A-Z]([a-zA-Z]+|[ÁÉÍÓÚáéíóíÑñ~\.])+| )+|[of|[0-9]+|\/|de|y|y])+'
    regla_identificacion = r'(N(\.)?I(\.)?T(\.)?|C(\.)?C(\.)?|T(\.)?J(\.)?|C(\.)?U(\.)?I(\.)?|R(\.)?U(\.)?C(\.)?|C(\.)?U(\.)?I(\.)?T(\.)?|DOCUMENTO|I(\.)?B(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|D(\.)?N(\.)?I(\.)?|R(\.)?U(\.)?T(\.)?|R(\.)?I(\.)?F(\.)?|S(\.)?A(\.)?T(\.)?|R(\.)?F(\.)?C(\.)?|R(\.)?T(\.)?N(\.)?|R(\.)?T(\.)?U(\.)?|S(\.)?S(\.)?N(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|IDENTIFICACION|R(\.)?U(\.)?N(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|RUC Nro)(\.| |:)(.)+?([0-9]+|\.|\,|\-){4,}'
    regla_vigencia = r'([Vv][i][Gg][Ee][Nsn][Cc][Ii][Aa]|D[Ee][Ss][Dd][Ee]|H[Aa][Ss][Tt][Aa]|I[Nn][Ii][Cc][Ii][Aa][Cc][Ii][OoÓó][NN]|T[Ee][Rr][Mm][Ii][Nn][Aa][Cc][Ii][OoÓó][Nn])(\.|:| )+?([0-9]+|/|-|\.| |:|[Dd][Ee]([Ll])?|\(\*\*\)|[Ee][Nn][Ee]([Rr][Oo])?|[Ff][Ee][Bb][Rr]([Ee][Rr][Oo])?|[Mm][Aa][Rr]([Zz][Oo])?|[Aa][Bb][Rr]([Ii][Ll])?|[Mm][Aa][Yy]([Oo])?|[Jj][Uu][Nn]([Ii][Oo])?|[Jj][Uu][Ll]([Ii][Oo])?|[Aa][Gg][Oo]([Ss][Tt][Oo])?|[Ss][Ee][Pp]([Tt][Ii][Ee][Mm][Bb][Rr][Ee])?|[Oo][Cc][Tt]([Uu][Bb][Rr][Ee])?|[Nn][Oo][Vv]([Ii][Ee][Mm][Bb][Rr][Ee])?|[Dd][Ii][Cc]([Ii][Ee][Mm][Bb][Rr][Ee])?|[Dd][IiÍí][Aa]|[Mm][Ee][Ss]|[Aa][Ññn][Oo]|[Hh][Oo][Rr][Aa]|[Aa]|[Ll][Aa][Ss]|[Hh][Aa][Ss][Tt][Aa]|[Hh][Ss]|[Hh][Rr][Ss]?|HH|Término|\(|\)|\/)+'
    regla_asegurador_aux = r'(?<= A[Gg][Ee][Nn][Tt][Ee])(:| )?([A-Z]|[ÁÉÍÓÚÑ]|Y| )+'
    Reglas_exception = [r'(([a-z]+?(\-|\.)+?[0-9]+)+[0-9]{4,}|[0-9]{2,})']
    #----------------si es empresa----------------#
    regla_empresa = ' (S(\.| )?E(\.| )?P(\.| )?|L(\.| )?T(\.| )?A(\.| )?|E(\.| )?S(\.| )?P(\.| )?|S(\.| )?A(\.| )?C(\.| )?|S(\.| )?A(\.| )?) '
    #----------------finaliza en----------------#
    regla_id= '(N(\.| )?I(\.| )?T(\.)?|C(\.)?C(\.)?|T(\.)?J(\.)?|C(\.)?U(\.)?I(\.)?|R(\.)?U(\.)?C(\.)?|C(\.)?U(\.)?I(\.)?T(\.)?|DOCUMENTO|I(\.)?B(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?|D(\.)?N(\.)?I(\.)?|R(\.)?U(\.)?T(\.)?|R(\.)?I(\.)?F(\.)?|S(\.)?A(\.)?T(\.)?|R(\.)?F(\.)?C(\.)?|R(\.)?T(\.)?N(\.)?|R(\.)?T(\.)?U(\.)?|S(\.)?S(\.)?N(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|N(\.)?I(\.)?T(\.)?E(\.)?|IDENTIFICACION|R(\.)?U(\.)?N(\.)?|C(\.)?U(\.)?I(\.)?L(\.)?)'
    regla_finaliza =  r'(ESP|DIRECCION|FECHA|TELEFONO|NIT)'
    regla_caracteres = '([:?%!¡\/]|GENERADO POR|RUC|NIT|[0-9]+|DIRECCION|FECHA|TELEFONO|NIT|KR|ORIGINAL|DEL SEGURO Y |A[Ss][Ee][Gg][Uu][Rr][Aa][Dd][Do][Ss]?|IDENTIFICACI[ÓO]N|Sección|Ramo|P[ÓóoO][Ll][Ii][Zz][Aa]|NOMBRE|TOMADOR)'

    #----------------Stopwords----------------#
    from stop_words import get_stop_words
    stop_words = get_stop_words('spanish')
    regla_stopwords = '[' + '|'.join(stop_words) + ']'
    #----------------reglas----------------#
    reglas = [regla_poliza, 
              regla_asegurado,regla_tomador, regla_beneficiario ,
              regla_identificacion,
              regla_vigencia,
              Reglas_exception,
              regla_empresa, regla_id, regla_finaliza,regla_caracteres,regla_stopwords]

    nom_reglas = [ 'Num_Poliza', 
                 'Asegurado', 'Tomador', 'Beneficiario',
                 'ID_Asegurado_Tomador_Benficiario',
                 'Fecha_Vigencia',
                  'Reglas_excepcion',
                 'regla_empresa', 'regla_id', 'regla_finaliza','regla_caracteres','regla_stopwords']
    
    REGLAS = pd.DataFrame({'nombre_regla': nom_reglas, 'regla': reglas})

    #borrar variables
    del reglas, regla_poliza, regla_asegurado,regla_tomador, regla_beneficiario , regla_identificacion, regla_vigencia, Reglas_exception, regla_empresa, regla_id, regla_finaliza,regla_caracteres,regla_stopwords
    del nom_reglas

    return(REGLAS)


#COMPILACION DE OTRAS REGLAS:
REGLAS_OTROS = Regla_Otra_Familia()

#FUNCION: Reglas de extraccion tomador asegurado y beneficiario
#OBJETIVO: Extraer del texto los participantes de la poliza: Tomador, Asegurado, Beneficiario.
#INPUT: Vector de texto
#OUPUT: Texto con el valor correspondiente.
def Reglas_tomador_asegura_beneficiario(valor):
    valor = [re.sub(REGLAS_OTROS.regla[9],'',x,re.I|re.M) for x in valor]
    valor = [re.sub(REGLAS_OTROS.regla[10],'',x,re.I|re.M) for x in valor]
    valor = [re.sub(REGLAS_OTROS.regla[11],'',x,re.I|re.M) for x in valor]
    valor = [x for x in valor if len(''.join(x.replace(" ", "")))>3]
    #Si es empresa:
    states = [re.search(REGLAS_OTROS.regla[7], x, re.I) != None for x in valor] #regla_empresa
    valor_return = np.nan
    if len(valor) >0 :
        if(any(states) == True):
            post = np.where(states)[0].tolist()
            valor = [ valor[i] for i in post]
            contador = collections.Counter(valor)
        
            unico_valor = contador.most_common()[0][0]
            unico_valor = unico_valor[0:re.search(REGLAS_OTROS.regla[7],unico_valor).end()-1] #regla_empresa
        else:
            contador = collections.Counter(valor)
            unico_valor = contador.most_common()[0][0]
        #quita espacios en blanco
        unico_valor = ' '.join(unico_valor.split())
        #muestra_valor unico 
        if(len(''.join(unico_valor.split()))) > 4:
            valor_return = unico_valor
    return valor_return

#FUNCION: Reglas de extracción del tipo de poliza
#OBJETIVO: Mediante ael uso de un diccionario predefinido (guardado en el path de Datos/información) 
#INPUT: Vector de texto
#OUPUT: Texto con el valor correspondiente al tipo de poliza.
tipos_poliza = pd.read_excel(path+'/Datos/Informacion/Diccionario de tipos de poliza.xlsx', sheet_name= 'Lista_tipo')
Tipos_poliza = unique(tipos_poliza.Tipo_1.tolist())
Tipos_poliza = [re.sub('á','[áa]',x) for x in Tipos_poliza]
Tipos_poliza = [re.sub('é','[ée]',x) for x in Tipos_poliza]
Tipos_poliza = [re.sub('í','[íi]',x) for x in Tipos_poliza]
Tipos_poliza = [re.sub('ó','[óo]',x) for x in Tipos_poliza]
def Tipo_de_poliza(textos):
    postition = [re.search(x, textos, re.I) for x in Tipos_poliza]
    postition = [x for x in postition if not pd.isnull(x)]
    if len(postition) > 0:
        conjuntos_tipo= pd.DataFrame({'Posicion' :[x.start()  for x in postition] ,'Nombre' :[x.group()  for x in postition] })
        conjuntos_tipo = conjuntos_tipo.sort_values('Posicion').reset_index(drop=True)
        if conjuntos_tipo.shape[0] > 1:
            if conjuntos_tipo['Posicion'][1] - conjuntos_tipo['Posicion'][0] < 100:
                return(conjuntos_tipo.loc[0, 'Nombre'] + ' & ' + conjuntos_tipo.loc[1, 'Nombre'])
            else:
                return(conjuntos_tipo.loc[0, 'Nombre'])
    else:
        return  'No se encontro'

#FUNCION: Reglas de patrón de identificación
#OBJETIVO: Aplica la regla de intendificación del agente seleccionado (Asegurado, beneficiario o tomador)
#INPUT: Valor del agente seleccionado y el texto de la poliza.
#OUPUT: Texto con el valor correspondiente.
def patron_identificacion(valores, textos):
    itera = re.finditer(valores, textos)
    indices_valor= [m.start(0) for m in itera]
    if len(indices_valor) > 1 :    
        kk = 0
        #print(indices_valor)
        valor_cc = evaluacion_regla(REGLAS_OTROS.regla[9] + '(:| )+?([0-9]+|\.|\,|\-([A-z]|[0-9]))+', documento= textos[0:indices_valor[0]] ,flaggs = re.I)#regla_identificacion
        while len(valor_cc) == 0:
            valor_cc = evaluacion_regla(REGLAS_OTROS.regla[9] +'(:| )+?([0-9]+|\.|\,|\-([A-z]|[0-9]))+' , textos[indices_valor[kk]:indices_valor[kk +1]] ,flaggs = re.I)#regla_identificacion
            kk += 1 
            if kk ==  len(indices_valor)-1:
                break
        if len(valor_cc) > 0:
            return(valor_cc[0])
        else:
            return  'No se encontro'
    else:
        return  'No se encontro'
    
    
#FUNCION:  Regla de identificación
#OBJETIVO: Extraer la identificación de cada uno de los agentes participantes de la poliza (Asegurado, beneficiario, tomador)
#INPUT:    Vectores con los resultados de extracción de (Asegurado, beneficiario, tomador) y texto
#OUPUT:    Vector con los resultados de identificación (ID_Asegurado, ID_Beneficiario, ID_Tomador)
def Regla_identificacion(valor_asegurado,valor_tomador,valor_beneficia, textos_buscar):
    #Declaracion de variables inicializadoras--------------------------------------
    nombres_valores = ['id_tomador','id_asegurado','id_beneficiario']
    valores_T_A_B =  [x for x in [valor_tomador, valor_asegurado, valor_beneficia] if not x == 'No se encontro']
    valor_cc =[]
    
    #Caso que no detecte ningun Asegurador, tomador o beneficiario------------------
    if len(valores_T_A_B) == 0:
        #print('entra en valore tab == 0')
        nombres_valores = ['regla_id_tomador']
        valor_cc = np.nan
        
    if len(valores_T_A_B) == 1:
        #print('entra en valores tab == 1')
        nombres_valores = ['regla_id_tomador']
        valor_cc = patron_identificacion(valores_T_A_B[0], textos_buscar)
        
    if len(valores_T_A_B) > 1:
        #print('entra en valores tab == 1')
        #encuentra la posicion.................... 
        posiciones_valor = []
        for value in valores_T_A_B :
            if pd.isna(re.search(value, textos_buscar)) == False:
                posiciones_valor.append(re.search(value, textos_buscar).end())
            else:
                posiciones_valor.append(np.nan)  
                
                
        #Organiza los valores x posición.............
        if len(unique(posiciones_valor)) == 1:            
            nombres_valores = ['regla_id_tomador']
            valor_cc = patron_identificacion(valores_T_A_B[0],textos_buscar)
        
        if len(unique(posiciones_valor)) > 1:
            #print('entra en posiciones_valor > 1')
            #si no encuentra alguna posicion
            if any(pd.isnull(posiciones_valor)) == True:
                post = np.where(np.isnan(posiciones_valor))[0].tolist()
                k = len(post)
                while k != 0:
                    #print(k)
                    postnew = np.where(np.isnan(posiciones_valor))[0].tolist()
                    nombres_valores.pop(postnew[0])
                    valores_T_A_B.pop(postnew[0])
                    posiciones_valor.pop(postnew[0])
                    k -=1
                del k , post
            #encuentra las identificadores............
            valores_T_A_B = [x for _,x in sorted(zip(posiciones_valor,valores_T_A_B))]
            nombres_valores = [x for _,x in sorted(zip(posiciones_valor,nombres_valores))]  
            for value in valores_T_A_B:
                valor_cc.append(patron_identificacion(value, textos_buscar))
    
    if len(nombres_valores) > 1:
        result = pd.DataFrame({'nombres_valores':nombres_valores,'valor_cc':valor_cc})
    else:
        result = pd.DataFrame({'valor_cc': [np.nan]})
    return(result)

#FUNCION: Caracteristicas Basica Otros
#OBJETIVO: Extrare los conjuntos basicos de las polizas que n pertenencena ninguna famlia.
#INPUT: Matriz de polizidas denominadas (OTRAS)
#OUPUT: 1. Matriz con las caracteristicas basicas en cada poliza.
#       2. Matriz de logs de las caracteristicas básicas en cada poliza. 
def Carct_Basicas_OTROS(Documents_OTRAS):
    for ii in range(Documents_OTRAS.shape[0]):
        texto = Documents_OTRAS.Textos[ii]
        Ubic_pol = Documents_OTRAS.nombres_doc[ii]
        Aseguradora = Documents_OTRAS.Aseguradora[ii]
        for kk in range(9):
            #-----REGLA NUMERO DE POLIZA -----#
            if kk == 0: 
                Num_poliza = 'No se encontro'
                Num_poliza_log = 'No se encontro sin ninguna regla.'
                if pd.isna(re.search(REGLAS_OTROS.regla[kk], texto)) == False:
                    poliza = re.search(REGLAS_OTROS.regla[kk], texto).group()
                    poliza =  re.sub('[A-z]+( |:|°|\.| )+','',poliza)
                    Num_poliza = poliza
                    Num_poliza_log = ''
                else:
                    regla_extra = '[Pp][oóOÓ][Ll][Ii][Zz][Aa](:|[Nn]|[Rr]|°|[0Oo]|\.| )+( |[A-z])+[0-9]{4,}'
                    if pd.isna(re.search( regla_extra, texto)) == False:
                        poliza = re.search(regla_extra, texto).group()
                        poliza = re.sub('[A-z]+( |:|°|\.| )+','',poliza)
                        Num_poliza = poliza
                        Num_poliza_log = 'Se aplico la segunda regla estándar.'
                    else:
                        Num_poliza =  'No se encontro'
                        Num_poliza_log = 'Se aplicaron 2 reglas estándar sin éxito.'

            #------REGLA ASEGURADO--------------#
            if kk == 1:
                Asegurado = 'No se encontro'
                Asegurado_log = 'No se encuentra bajo ninguna regla.'
                if pd.isna(re.search(REGLAS_OTROS.regla[kk],texto)) == False:           
                    asegurado = re.search(REGLAS_OTROS.regla[kk],texto).group()
                    asegurado = re.sub('.+:','',asegurado)
                    asegurado = re.sub('[aA][Ss][Ee][Gg][Uu][Rr][Aa][Dd][Oo]','',asegurado)
                    asegurado = re.sub(REGLAS_OTROS.regla[9],'',asegurado,re.I|re.M) #regla_finaliza
                    asegurado = re.sub(REGLAS_OTROS.regla[10],'',asegurado,re.I|re.M)# regla_caracteres
                    asegurado = re.sub(REGLAS_OTROS.regla[11],'',asegurado,re.I|re.M)#regla_stopwords
                    asegurado = re.sub(REGLAS_OTROS.regla[8] +'.+','',asegurado)#regla_id
                    asegurado_prueb = re.sub('\s+','',asegurado)
                    if len(asegurado_prueb)> 3 :
                        Asegurado = asegurado
                        Asegurado_log = 'Se aplico la primera regla.'
                    else:
                        valor_asegurado = evaluacion_regla(REGLAS_OTROS.regla[kk],texto,flaggs= re.M)
                        if len(valor_asegurado) ==  0:
                            Asegurado = 'No se encontro'
                            Asegurado_log = 'Se aplico la segunda regla sin éxito.'
                        else:
                            valor_asegurado =[re.sub('(DATOS DEL|A[Ss][Ee][Gg][Uu][Rr][Aa][Dd][Oo][Rr]?|NOMBRE|PRINCIPAL|ASEGURA A|Nombre y Apellido|:)+','',x, re.I) for x in valor_asegurado]
                            if len(valor_asegurado) == 1:
                                Asegurado = Reglas_tomador_asegura_beneficiario(valor_asegurado)
                                Asegurado_log = 'Se aplico la segunda regla con éxito.'
                            if len(valor_asegurado) > 1:
                                asegurado_prueb = [ re.sub('\s+','', asegurado) for asegurado in valor_asegurado]
                                for post in range(len(asegurado_prueb)):
                                    if len(asegurado_prueb[post]) > 0:
                                        Asegurado = valor_asegurado[post]
                                        Asegurado_log = 'Se aplico la tercera regla con éxito.'
                    
           #------REGLA TOMADOR-------------#
            if kk == 2:
                Tomador ='No se encontro.'
                Tomador_log = 'No se encontro bajo ninguna regla.'
                if pd.isna(re.search(REGLAS_OTROS.regla[kk],texto)) == False:
                    tomador = re.search(REGLAS_OTROS.regla[kk],texto).group()
                    tomador= re.sub('.+:','', tomador)
                    tomador = re.sub('([Tt][Oo][Mm][aA][dD][oO][rR]|C[Ll][Ii][Ee][Nn][Tt][Ee]|[Cc][Oo][Nn][Tt][Rr][Aa][Tt][Aa][Nn][Tt][Ee]|P[Rr][Oo][Pp][Oo][Nn][Ee][Nn][Tt][Ee])','', tomador)
                    tomador = re.sub(REGLAS_OTROS.regla[9],'', tomador,re.I|re.M) #regla_finaliza
                    tomador= re.sub(REGLAS_OTROS.regla[10],'', tomador,re.I|re.M)# regla_caracteres
                    tomador = re.sub(REGLAS_OTROS.regla[11],'', tomador,re.I|re.M)#regla_stopwords
                    tomador = re.sub(REGLAS_OTROS.regla[8] +'.+','',tomador)#regla_id
                    tomador_prueb = re.sub('\s+','',tomador)
                    if len(tomador_prueb) <3:
                        valor = evaluacion_regla(REGLAS_OTROS.regla[kk],texto,flaggs= re.M)
                        if len(valor)> 1:
                            valor =[re.sub('.+:','',x, re.I) for x in valor]
                            valor =[Reglas_tomador_asegura_beneficiario(valores) for valores in valor]
                            if len(valor) == 1:
                                Tomador = valor
                                Tomador_log = 'Se encuentra aplicando la primera regla.'
                            else:
                                tomador_prueb = re.sub('\s+','',tomador)
                                for post in range(len(tomador_prueb)):
                                    if len(tomador_prueb) > 1:
                                        Tomador = tomador_prueb 
                                        Tomador_log = 'Se encuentra aplicando la segunda regla.'
                    else:
                        Tomador = tomador
                        Tomador_log = ''

            #------REGLA BENEFICACIO-------------#
            if kk == 3:
                Beneficiario = 'No se encontro'
                Beneficiario_log = 'No se encuentra aplicando la primera regla.'
                if pd.isna(re.search(REGLAS_OTROS.regla[kk],texto)) == False:
                    beneficiario = re.search(REGLAS_OTROS.regla[kk],texto).group()
                    beneficiario = re.sub('.+:','', beneficiario)
                    beneficiario = re.sub(REGLAS_OTROS.regla[9],'', beneficiario,re.I|re.M) #regla_finaliza
                    beneficiario = re.sub(REGLAS_OTROS.regla[10],'', beneficiario,re.I|re.M)# regla_caracteres
                    beneficiario = re.sub(REGLAS_OTROS.regla[11],'', beneficiario,re.I|re.M)#regla_stopwords
                    beneficiario = re.sub(REGLAS_OTROS.regla[8] +'.+','',beneficiario)#regla_id
                    beneficiario_prueb = re.sub('\s+','',beneficiario)
                    if len(beneficiario_prueb) < 3:
                        Beneficiario =  'No se encontro'
                    else:
                        Beneficiario = beneficiario
            #------REGLA ID: ASEGURADO, TOMADOR, BENEFICIARIO --------------#
            if kk == 4:
                Identificadores = Regla_identificacion(Asegurado, Tomador, Beneficiario, texto)
                #Valore base
                ID_Asegurado =  'No se encontro'
                ID_Tomador =  'No se encontro'
                ID_Beneficiario =  'No se encontro'
                ID_Asegurado_log =  'No se encuentra aplicando la primera regla.'
                ID_Tomador_log =  'No se encuentra aplicando la primera regla.'
                ID_Beneficiario_log =  'No se encuentra aplicando la primera regla.'
                #valores detectados
                if Identificadores.shape != (1,1):
                    for dim_id  in range(Identificadores.shape[0]):
                        if Identificadores.nombres_valores[dim_id] == 'id_asegurado':
                            ID_Asegurado = Identificadores.valor_cc[dim_id]
                            ID_Asegurado_log = ''
                        if Identificadores.nombres_valores[dim_id] == 'id_tomador':
                            ID_Tomador = Identificadores.valor_cc[dim_id]
                            ID_Tomador_log = ''
                        if Identificadores.nombres_valores[dim_id] == 'id_beneficiario':
                            ID_Beneficiario = Identificadores.valor_cc[dim_id]
                            ID_Beneficiario_log = ''

             #------FECHAS DE VIGENCIA--------------#
            if kk == 5:
                valores = evaluacion_regla(regex=REGLAS_OTROS.regla[kk]+ '[0-9]{4}', documento= texto,flaggs= re.M) #regla_vigencia
                valores = unique(valores)
                postition = np.where([bool(re.search('(1[0-9]|20)[0-9]{2}',x)) for x in valores])
                valores = [valores[ii] for  ii in postition[0]]

                Fecha_vigencia_Hasta = 'No se encontro'
                Fecha_vigencia_Hasta_log = 'No se encuentra aplicando la primera regla.'
                Fecha_vigencia_Desde = 'No se encontro'
                Fecha_vigencia_Desde_log = 'No se encuentra aplicando la primera regla.'

                if len(valores) > 1:
                    Fecha_vigencia_Hasta = valores[0]
                    Fecha_vigencia_Desde = valores[1]
                if len(valores) == 1:
                    Fecha_vigencia_Hasta = valores[0]
                    Fecha_vigencia_Desde = ''
            #------REGLA MONEDA EMITIDA --------------#
            if kk == 6:
                regla_Moneda = r'(M[Oo][Nn][Ee][Dd][Aa]|E[mM][iI][tT][iI][dD][dA] [eE][nN])( |:)+([A-Z][A-z]+|[ÁÉÍÓÚÑ]|\.|-|$)+'
                if pd.isna(re.search(regla_Moneda,texto)) == False:
                    Emitida = re.search(regla_Moneda,texto).group()
                    Emitida_log = ''
                else:
                    Emitida = 'No se encontro'
                    Emitida_log = 'No se encuentra aplicando la primera regla.'
            #------REGLA TIPO DE POLIZA--------------#
            if kk == 7:
                Tipo = Tipo_de_poliza(texto)
                Tipo_log = 'Se aplica diccionario.'
            #------REGLA CERTIFICADO --------------#
            if kk == 8:
                Certificado =  'No se encontro'
                Certificado_log = 'No se encuentra aplicando la primera regla.'
                regla_Certificado = r'(C[Ee][Rr][Tt][Ii][Ff][Ii][Cc][Aa][Dd][Oo])( |:)+([0-9]+|-|\.)+'
                if pd.isna(re.search(regla_Certificado,texto)) == False:
                    Certificado  = re.search(regla_Certificado,texto).group()
                    Certificado_log = ''
        #------CONSTRUCCION BASE --------------#
        if ii == 0:
            BASE_OTROS = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                          'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                          'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta],
                                          'Moneda_Poliza':[Emitida],
                                          'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                          'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]}) 
            BASE_OTROS_LOG = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                          'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                          'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log],
                                          'Moneda_Poliza':[Emitida_log],
                                          'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                          'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]}) 
        else:
            base  = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                          'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza],'Certificado':[Certificado] ,'Tipo_Poliza':[Tipo], 
                                          'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta],
                                          'Moneda_Poliza':[Emitida], 'Tomador':[Tomador],'ID_Tomador':[ID_Tomador], 
                                          'Asegurado':[Asegurado],'ID_Asegurado':[ID_Asegurado]}) 
            
            base_log = pd.DataFrame({'Nombre_Poliza': [Ubic_pol], 
                                          'Aseguradora':[Aseguradora], 'Num_Poliza':[Num_poliza_log],'Certificado':[Certificado_log] ,'Tipo_Poliza':[Tipo_log], 
                                          'Fecha_Inicio_Vigencia':[Fecha_vigencia_Desde_log], 'Fecha_Fin_Vigencia':[Fecha_vigencia_Hasta_log],
                                          'Moneda_Poliza':[Emitida_log],
                                          'Tomador':[Tomador_log],'ID_Tomador':[ID_Tomador_log], 
                                          'Asegurado':[Asegurado_log],'ID_Asegurado':[ID_Asegurado_log]}) 
            
            BASE_OTROS = pd.concat([BASE_OTROS, base])
            BASE_OTROS_LOG = pd.concat([BASE_OTROS_LOG, base_log])
    return(BASE_OTROS, BASE_OTROS_LOG)

#FUNCION:  Extraccion de Riesgos
#OBJETIVO: Extraer las principales caraceristicas de los riesgos en las polizas denomiadas como otras.
#INPUT: Matriz de polizidas denominadas (OTRAS)
#OUPUT: 1. Matriz con los riesgos encontrados en cada poliza.
#       2. Matriz de logs de los riesgos en cada poliza.
def Extraccion_riesgos(Documents_OTRAS):
    regla_pais = ' (C[Oo][Ll][Oo][Mm][Bb][Ii][Aa]|P[eE][rR][uúUÚ]|E[cC][uU][aA][dD][oO][rR]|A[rR][gG][eE][nN][tT][iI][nN][aA]|B[oO][lL][iI][vV][iI][aA]|B[rR][aA][sS][iI][lL]|C[hH][iI][lL][eE]|C[uU][bB][aA]|E[lL] S[aA][lL][vV][aA][dD][oO][rR]|G[uU][aA][tT][eE][Mm][aA][lL][aA]|H[oO][nN][dD][uU][rR][aA][sS]|M[ÉEeé][xX][iI][Cc][oO]|N[iI][cC][aA][rR][aA][gG][uU][aA]|P[aA][nN][aA][mM][aáAÁ]|P[aA][rR][aA][gG][uU][aA][yY]|P[uU][eE][rR][tT][oO] R[iI][cC][oO]|U[rR][uU][gG][uU][aA][yY]|V[eE][nN][eE][zZ][uU][eE][lL][aA])'
    for ii in range(Documents_OTRAS.shape[0]):
        textos = Documents_OTRAS.Textos[ii]
        Nombre_Poliza =  Documents_OTRAS.nombres_doc[ii]
        
        #------Regla de Pais --------------#
        if pd.isna(re.search(regla_pais,textos)) == False:
            value_pais = [x.strip(' ') for x in evaluacion_regla(regla_pais, textos, re.I|re.M)]
            value_pais = collections.Counter(value_pais)
            Pais = value_pais.most_common()[0][0]
            Pais_log = ''
        else:
            Pais = 'No se encontro'
            Pais_log = 'No se encuentra, se aplico diccionario.'
            
         #------Reglas de riesgo y ubilicacion del riesgo --------------#
        regla_riesgo= r'(((D[Ee][Ss][Cc][Rr][Ii][Pp][Cc][Ii][OoÓó][Nn] |C[lL][aA][sS][eE] |[Dd][Ee][lL] )*R[iI][Ee][Ss][Gg][Oo]|T[iI][Pp][Oo]|Actividad|(BIEN |PROYECTO |MATERIA )ASEGURAD[AO](-ACTIVIDAD\/OCUPACIÓN)?|Descripción del proyecto|Objeto asegurado|Descripción del bien)+)'
        regla_ubicacion = r'(UBICACI[ÓO]N( DEL RIESGO)?|Direcci[oó]n del riesgo|Ubic. Riesgo)'
        
        reg_ub_uno = regla_riesgo + ':' +'( |\.|[A-Za-z]+|[ÁÉÍÓÚÑáéíóúñ]|[/\-,])+'
        reg_ub_dos = regla_ubicacion + '[:\.]'+'( |\.|[A-Za-z]+|[ÁÉÍÓÚÑáéíóúñ]|[/\-,\.]|[0-9]+)+'
        #Caso con dos puntos (:)---------------------------------------------------------------
        # ubicaciones riesgo
        itera = re.finditer(reg_ub_dos, textos, re.M)
        indices_ubicacion_start = [m.start(0) for m in itera]
        itera = re.finditer(reg_ub_dos, textos, re.M)
        indices_ubicacion_end = [m.end(0) for m in itera]
        
        #inicializa las variables:
        ubicacion_riesgo_log = 'No se encontro'
        if len(indices_ubicacion_start) > 0:
            if len(indices_ubicacion_start) == len(indices_ubicacion_end):
                ubicacion_riesgo = [textos[indices_ubicacion_start[jj]:indices_ubicacion_end[jj]] for jj in range(len(indices_ubicacion_end))]            
            else:
                long = min(len(indices_riesgo_start), len(indices_riesgo_end))
                ubicacion_riesgo = [textos[indices_ubicacion_start[jj]:indices_ubicacion_end[jj]] for jj in range(long)]
                ubicacion_riesgo_log = ''
        else:
                ubicacion_riesgo = ['']
                ubicacion_riesgo_log = 'No se encuentra aplicando la primera regla.'
        
         #------REGLA Tipo de riesgo--------------#
        itera_riesgo = re.finditer(reg_ub_uno, textos, re.M)
        indices_riesgo_start = [m.start(0) for m in itera_riesgo]

        itera_riesgo = re.finditer(reg_ub_uno, textos, re.M)
        indices_riesgo_end= [m.end(0) for m in itera_riesgo]

        if len(indices_riesgo_start) > 0:
            if len(indices_riesgo_start) == len(indices_riesgo_end):
                tipo_riesgo = [textos[indices_riesgo_start[jj]:indices_riesgo_end[jj]] for jj in range(len(indices_riesgo_end))]  
                tipo_riesgo_log = ''
                ubicacion_riesgo_log = ''
            else:
                long = min(len(indices_riesgo_start), len(indices_riesgo_end))
                tipo_riesgo = [textos[indices_riesgo_start[jj]:indices_riesgo_end[jj]] for jj in range(long)] 
                tipo_riesgo_log = ''
                ubicacion_riesgo_log = 'Se encuentran mas ubicaciones que tipos de riesgo.'
        else:
            tipo_riesgo = ['']
            tipo_riesgo_log = 'No se encuentra aplicando la primera regla.'

        #En caso de que no se hayan encontrado todos los valores
        if len(ubicacion_riesgo) != len(tipo_riesgo):
            long = max(len(ubicacion_riesgo) , len(tipo_riesgo))
            if len(ubicacion_riesgo) < long:
                ubicacion_riesgo =  ubicacion_riesgo+['']*abs(len(tipo_riesgo) -len(ubicacion_riesgo))
                ubicacion_riesgo_log =''
            if len(ubicacion_riesgo) >  long:
                ubicacion_riesgo = ubicacion_riesgo
                ubicacion_riesgo_log = ''
            if len(tipo_riesgo) < long:
                tipo_riesgo =  tipo_riesgo+['']*abs(len(tipo_riesgo) - len(ubicacion_riesgo))
                tipo_riesgo_log = ''  
            if len(tipo_riesgo) < long:
                tipo_riesgo = tipo_riesgo  
                tipo_riesgo_log = ''
                ubicacion_riesgo_log = ''
        #construccion de matriz
        base_riesgo = pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza]*len(tipo_riesgo),
                                        'Riesgo_ID':tipo_riesgo, 
                                        'Direccion_Riesgo':ubicacion_riesgo,
                                        'Pais': [Pais]*len(tipo_riesgo),
                                         #Segementación de la ubicación : Trabajo Futuro.#
                                        'Descrip_Riesg_1':['']*len(tipo_riesgo), 'Descrip_Riesg_2':['']*len(tipo_riesgo) ,
                                        #-------------------------------------------------#
                                        'Division_Administrativa_1':['']*len(tipo_riesgo),
                                        'Division_Administrativa_2': ['']*len(tipo_riesgo),
                                        'Division_Administrativa_3': ['']*len(tipo_riesgo),
                                         #Segementación de la ubicación : Trabajo Futuro.
                                        'Codigo_Postal': ['']*len(tipo_riesgo), 'Latitud': ['']*len(tipo_riesgo), 'Longitud': ['']*len(tipo_riesgo)
                                           })
        #construccion de matriz_log
        base_riesgo_log = pd.DataFrame({'Nombre_Poliza':[Nombre_Poliza],
                                        'Riesgo_ID':tipo_riesgo_log, 'Direccion_Riesgo':ubicacion_riesgo_log, 'Pais': [Pais_log],
                                         #Segementación de la ubicación : Trabajo Futuro.#
                                        'Descrip_Riesg_1':[''], 'Descrip_Riesg_2':[''] ,
                                        #-------------------------------------------------#
                                        'Division_Administrativa_1':[''],'Division_Administrativa_2': [''],'Division_Administrativa_3': [''],
                                         #Segementación de la ubicación : Trabajo Futuro.
                                        'Codigo_Postal': [''], 'Latitud': [''], 'Longitud': ['']
                                           })

        if ii == 0:
            BASE_RIESGO = base_riesgo
            BASE_RIESGO_LOG = base_riesgo_log
            del base_riesgo , base_riesgo_log
        else:
            BASE_RIESGO = pd.concat([BASE_RIESGO,base_riesgo])
            BASE_RIESGO_LOG = pd.concat([BASE_RIESGO_LOG,base_riesgo_log])
            del base_riesgo , base_riesgo_log

    return(BASE_RIESGO, BASE_RIESGO_LOG)
      
#FUNCION:  General otras Coberturas
#OBJETIVO: Extrae todas las coberturas encontradas en las polizas de un diccionario predefinido.
#          Las coberturas no se encuentran no asociadas a un riesgo en particular.
#INPUT:    Matriz documentos de otras Polizas.
#OUPUT:    1. Matriz con los resultado de extracción de otras polizas.
#          2. Matriz de logs (errores) asociados a las coberturas.
def GENERAL_OTRAS_COBERTURAS(Documents_OTRAS):
    for ii in range(Documents_OTRAS.shape[0]):
        textos = Documents_OTRAS.Textos[ii]
        Nombre_Poliza =  Documents_OTRAS.nombres_doc[ii]
        #cobertura
        if pd.isna(re.search(Coberturas, textos, re.M)) == False:
            cobertura_texto = re.finditer(pattern = Coberturas, string= textos, flags= re.M)
            cobertura_texto = unique([x.group() for x in cobertura_texto])
            cobertura_texto = ' - '.join(cobertura_texto)
            cobertura_texto_log = ''
        else:
            cobertura_texto = 'No se encontro' 
            cobertura_texto_log = 'No se encontro ningún elemento del diccinario de coberturas.'
        #-----------Construye la matriz---------------#
        if ii == 0:
            Base_Cobertura =  pd.DataFrame({ 'Nombre_Poliza':Nombre_Poliza,
                                'Cobertura':[cobertura_texto], 'Prima_Cobertura':[''],'Valor_Cobert':[''], 
                                'Valor_Cobertura_%':[''],'Valor_Cobertura_Lucro_cesante_(dias)':[''],'Base_Valor_Cobertura_%':[''], 'Moneda_Prima_Cobertura':['']})
            
            Base_Cobertura_log =  pd.DataFrame({ 'Nombre_Poliza':Nombre_Poliza,
                                'Cobertura':[cobertura_texto_log], 'Prima_Cobertura':[''],'Valor_Cobert':[''], 
                                'Valor_Cobertura_%':[''],'Valor_Cobertura_Lucro_cesante_(dias)':[''],'Base_Valor_Cobertura_%':[''], 'Moneda_Prima_Cobertura':['']})
        else:
            Base_cobert =  pd.DataFrame({ 'Nombre_Poliza':Nombre_Poliza,
                                'Cobertura':[cobertura_texto], 'Prima_Cobertura':[''],'Valor_Cobert':[''], 
                                'Valor_Cobertura_%':[''],'Valor_Cobertura_Lucro_cesante_(dias)':[''],'Base_Valor_Cobertura_%':[''], 'Moneda_Prima_Cobertura':['']})
            Base_cobert_log =  pd.DataFrame({ 'Nombre_Poliza':Nombre_Poliza,
                                'Cobertura':[cobertura_texto_log], 'Prima_Cobertura':[''],'Valor_Cobert':[''], 
                                'Valor_Cobertura_%':[''],'Valor_Cobertura_Lucro_cesante_(dias)':[''],'Base_Valor_Cobertura_%':[''], 'Moneda_Prima_Cobertura':['']})
    
            Base_Cobertura = pd.concat([Base_Cobertura,Base_cobert])
            Base_Cobertura_log = pd.concat([Base_Cobertura_log,Base_cobert_log])
    return(Base_Cobertura , Base_Cobertura_log)

#FUNCION:Genral_OTRAS_polizas
#OBJETIVO: Extrae la información general de  las polizas , como: Lucro cesante, totales, deducibles, limites, etc.
#INPUT: Matriz de documentos de otras polizas.
#OUPUT: 1. Matriz de valores generales de las polizas.
#       2. Matriz de logs (errores) de los valores generales de las polizas.
def Genral_OTRAS_polizas(Documents_OTRAS):
    for ii in range(Documents_OTRAS.shape[0]):
        texto = Documents_OTRAS.Textos[ii]
        Nombre_Poliza = Documents_OTRAS.nombres_doc[ii]
        regla_divisiones = r'\.\s+[A-Z][A-z]+'
        regla_cantidades = r'([0-9]+|\.|,|\s|o|$)+'
        #----------lucro cesante------------------------#
        regla_lucro = r'\.\s+(L[uU][Cc][Rr][Oo]( |-)+?[Cc][Ee][Ss][Aa][Nn][Tt][Ee]|P[Ee][Rr][Jj][Uu][Ii][Cc][Ii][Oo]( )+[Pp][Oo][Rr]( )+[Pp][aa][Rr][Aa][Ll][Ii][Zz][Aa][Cc][Ii][OoóÓ][Nn]|P[ÉéeE][Rr][Dd][Ii][Dd][Aa]( )+[Dd][Ee]( )+[Bb][Ee][Nn][Ee][Ff][Ii][Cc][Ii][Oo])( |:)+'
        Lucro_Cesante =  'Lucro Cesante: No se encontro'
        Lucro_Cesante_log = 'Lucro cesante: No se encontro con la primera regla.'
        if pd.isna(re.search(regla_lucro, texto)) == False:
            ini_lucro = re.search(regla_lucro, texto).span()
            end_lucro = re.search(regla_divisiones, texto[ini_lucro[1]:]).span()[0]
            Lucro_Cesante = 'Lucro cesante:' + texto[ini_lucro[0]: ini_lucro[1] + end_lucro +1]
            Lucro_Cesante_log = ''
       #----------Dedulcible-----------------------------#
        regla_deducible = r'\.\s+((F[rR][aA][nN][qQ][uU][iI][cC][iI][aA][sS]?\s|M\.?)?(D[eE][Dd][Uu][Cc][Ii][Bb][Ll][Ee][Ss]?|F[rR][aA][nN][qQ][uU][iI][cC][iI][aA][sS]?))\s+?( |:)+(\(([A-z]|[0-9])+\))\s+[A-Z][A-z]+'
        Deducible = 'No se encontro'
        Deducible_log = 'No se encontro con la primera regla.'
        if pd.isna(re.search(regla_deducible, texto)) == False:
            ini_dedu =re.search(regla_deducible, texto).span()
            end_dedu = re.search(regla_divisiones, texto[ini_dedu[1]:]).span()[0]
            Deducible = texto[ini_dedu[0]: ini_dedu[1] + end_dedu +1]
            Deducible_log = ''
        #----------limites--------------------------#
        regla_limite = r'\.\s+L[íÍiI][Mm][Ii][Tt][Ee]\s*(ASEGURADO|[ÚUuú]NICO Y COMBINADO|\s+[dD][Ee]\s+[Ii][Nn][Dd][Ee][Mm][Nn][Ii][Zz][Aa][Cc][Ii][oOÓó][Nn]|[Ii][Nn][Dd][Ee][Mm][Nn][Ii][Zz][Aa][A-z]+)+:?'
        Limites = 'No se encontro'
        Limites_log = 'No se encontro con la primera regla.'
        if pd.isna(re.search(regla_limite, texto)) == False:
            ini_limite =re.search(regla_limite, texto).span()
            end_limite = re.search(regla_divisiones, texto[ini_limite[1]:]).span()[0]
            Limites = texto[ini_limite[0]: ini_limite[1] + end_limite +1]
            Limites_log = ''
        #----------total prima----------#
        regla_T_Prima = r'(T[oO][Tt][Aa][Ll]|M[oO0][nN][Tt][Oo0]|S[Uu][Mm][Aa])\s+[Pp][Rr][Ii][Mm][Aa]( |:)+'
        Prima_Total = 'No se encontro'
        Prima_Total_log = 'No se encontro con la primera regla.'
        if pd.isna(re.search(regla_T_Prima , texto)) == False:
            ini_prima = re.search(regla_T_Prima , texto).span()
            end_prima = re.search(regla_divisiones, texto[ini_prima[1]:]).span()[0]
            texto_prima  = texto[ini_prima[0]: ini_prima[1] + end_prima +1]
            if pd.isna(re.search(regla_cantidades, texto_prima)) == False:
                Prima_Total = re.search(regla_cantidades, texto_prima).group()
                Prima_Total = ''
            else:
                Prima_Total = texto_prima
                Prima_Total_log = 'No se pudo extraer una cantidad en especifico.'
                
        #----------Total asegurado ----------#   
        regla_T_asegurado = r'(T[oO][Tt][Aa][Ll]|M[oO0][nN][Tt][Oo0]|S[Uu][Mm][Aa])\s+[Aa][Ss][Ee][Gg][Uu][Rr][Aa][Dd][Oo]( |:)+' 
        Total_Asegurado = 'No se encontro'
        Total_Asegurado_log = 'No se encontro con la primera regla.'
        if pd.isna(re.search(regla_T_asegurado, texto)) == False:
            ini_asegurado =re.search(regla_T_asegurado, texto).span()
            end_asegurado = re.search(regla_divisiones, texto[ini_asegurado[1]:]).span()[0]
            texto_Asegurado = texto[ini_asegurado[0]: ini_asegurado[1] + end_asegurado +1]
            if pd.isna(re.search(regla_cantidades, texto_Asegurado)) == False:
                Total_Asegurado = re.search(regla_cantidades, texto_Asegurado ).group()
                Total_Asegurado_log = ''
            else:
                Total_Asegurado = texto_Asegurado
                Total_Asegurado_log = 'No se pudo extraer una cantidad en especifico.'
        #----------Total asegurable----------#
        regla_T_asegurable = r'(T[oO][Tt][Aa][Ll]|M[oO0][nN][Tt][Oo0]|S[Uu][Mm][Aa])\s+[Aa][Ss][Ee][Gg][Uu][Rr][Aa][Bb][lL][Ee]( |:)+'
        Total_Asegurable =  'No se encontro'
        Total_Asegurable_log = ''
        if pd.isna(re.search(regla_T_asegurable, texto)) == False:
            ini_asegurable =re.search(regla_T_asegurable, texto).span()
            end_asegurable = re.search(regla_divisiones, texto[ini_asegurable[1]:]).span()[0]
            Total_Asegurable = texto[ini_asegurable[0]: ini_asegurable[1] + end_asegurable +1]
            Total_Asegurable_log = ''
        #-----------participacion---------------#
        regex_coaseguradora  = 'Coaseguradora(s)?'
        if pd.isna(re.search(regex_coaseguradora, texto)) == False:
            indx = re.search(regex_coaseguradora,texto).end()
            Participacion = re.search('[0-9]+([0-9]+|\.|,)+',texto[indx:], re.I).group()
            Participacion_log = ''
        else: 
            Participacion = 'No se encontro'
            Participacion_log ='No se encuentra aplicando la regla general.'
           
        #----------construcción matriz ----------#
        if ii == 0:
            Base_general = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                         #----------------#
                                         'Cobertura': [Lucro_Cesante], 'Limites_raw': [Limites], 
                                         'Total_Asegurado': [Total_Asegurado],'Total_Asegurable': [Total_Asegurable], 'Total_Prima': [Prima_Total],
                                         'Moneda_Prima' :[''], 'Moneda_Total_Asegurable' :[''], 'Moneda_Total_Asegurado' :[''],
                                         #----------------#
                                         'Participacion' : Participacion
                                   })
            Base_general_log = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible_log], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                         #----------------#
                                         'Cobertura': [Lucro_Cesante_log],'Limites_raw': [Limites_log], 
                                         'Total_Asegurado': [Total_Asegurado_log],'Total_Asegurable': [Total_Asegurable_log], 'Total_Prima': [Prima_Total_log],
                                        'Moneda_Prima' :[''], 'Moneda_Total_Asegurable' :[''], 'Moneda_Total_Asegurado' :[''],
                                         #----------------#
                                         'Participacion' : Participacion_log
                                   })
        else: 
            base = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                         #----------------#
                                         'Cobertura': [Lucro_Cesante],'Limites_raw': [Limites], 
                                         'Total_Asegurado': [Total_Asegurado],'Total_Asegurable': [Total_Asegurable], 'Total_Prima': [Prima_Total],
                                         'Moneda_Prima' :[''], 'Moneda_Total_Asegurable' :[''], 'Moneda_Total_Asegurado' :[''],
                                         #----------------#
                                         'Participacion' :Participacion
                                   })
            base_log = pd.DataFrame({ 'Nombre_Poliza': [Nombre_Poliza], 
                                         'Deducibles_raw' : [Deducible_log], 
                                        #Segementación de la ubicación : Trabajo Futuro.
                                         'Deducible_%': [''],'Base_Deducible_%': [''], 'Deducible_$': [''] ,'Moneda_Deducible_$' : [''],
                                         'Deducible_Lucro_Cesante_(dias)': [''], 'Deducible_Min_$': [''], 'Moneda_Deducible_Min_$': [''],
                                         #----------------#
                                         'Cobertura': [Lucro_Cesante_log],'Limites_raw': [Limites_log], 
                                         'Total_Asegurado': [Total_Asegurado_log],'Total_Asegurable': [Total_Asegurable_log], 'Total_Prima': [Prima_Total_log],
                                         'Moneda_Prima' :[''], 'Moneda_Total_Asegurable' :[''], 'Moneda_Total_Asegurado' :[''],
                                         #----------------#
                                         'Participacion' : Participacion_log
                                   })
            Base_general = pd.concat([Base_general, base])
            Base_general_log = pd.concat([Base_general_log, base_log])
    return(Base_general, Base_general_log)

        
        
#--------------------------------------------------------#
#----- 3 LLAMAR LAS FUNCIONES DE FAMILIA-----------------#
#--------------------------------------------------------#
#FUNCION: Analisis_total
#OBJETIVO: Extraer, identificar y unir, las caracteristicas de las polizas por tipo de familia determinada.
#INPUT: Matriz de polizas que contiene [Nombre documento, Texto, Aseguradora]
#OUPUT: Matriz unificada (para todas las polizas) que compila los resultados.
#NOTA: Esta función esta en construcción por favor no modificar y adaptarse al output.
def Analisis_total(documentos):
    Lista_Familias = []
    Lista_Familias_log =[]
   #------------FAMILIA HDI------------#
    Documents_HDI = documentos[documentos.Aseguradora == 'HDI SEGUROS']
    if Documents_HDI.shape[0] >0:
        Documents_HDI = Documents_HDI.reset_index()
        del Documents_HDI['index']
        Base_Inicio_HDI,Base_Inicio_HDI_log  = Carct_Basicas_HDI(Documents_HDI)
        Base_Riesgo_HDI,Base_Riesgo_HDI_log = Caract_Riesgos_HDI(Documents_HDI)
        Base_Cobert_HDI,Base_Cobert_HDI_log = Caract_Coberturas_HDI(Documents_HDI)
        Base_Genral_HDI,Base_Genral_HDI_log = Caract_Generales_HDI(Documents_HDI)
        Base_General_HDI_dos,Base_General_HDI_dos_log = Caract_Generales_HDI_Dos(Documents_HDI)

        RESULTADOS_POLIZA_HDI = pd.merge(Base_Riesgo_HDI, Base_Cobert_HDI, on = 'Nombre_Poliza')
        Base_General_HDI_dos = pd.merge (Base_Genral_HDI, Base_General_HDI_dos, on = 'Nombre_Poliza')
        RESULTADOS_POLIZA_HDI = pd.concat([RESULTADOS_POLIZA_HDI, Base_General_HDI_dos] , sort=False)
        RESULTADOS_POLIZA_HDI = pd.merge(Base_Inicio_HDI,RESULTADOS_POLIZA_HDI ,on= 'Nombre_Poliza')
        RESULTADOS_POLIZA_HDI = RESULTADOS_POLIZA_HDI.sort_values(by = ['Nombre_Poliza','Riesgo_ID'] )
        
        #log_familia_HDI
        RESULTADOS_POLIZA_HDI_log = pd.merge(Base_Riesgo_HDI_log, Base_Cobert_HDI_log, on = 'Nombre_Poliza')
        Base_General_HDI_dos_log = pd.merge (Base_Genral_HDI_log, Base_General_HDI_dos_log, on = 'Nombre_Poliza')
        RESULTADOS_POLIZA_HDI_log = pd.concat([RESULTADOS_POLIZA_HDI_log, Base_General_HDI_dos_log] , sort=False)
        RESULTADOS_POLIZA_HDI_log = pd.merge(Base_Inicio_HDI_log,RESULTADOS_POLIZA_HDI_log ,on= 'Nombre_Poliza')
        #Añade a la lista de familias:
        Lista_Familias.append(RESULTADOS_POLIZA_HDI)
        Lista_Familias_log.append( RESULTADOS_POLIZA_HDI_log )
        
    #------------FAMILIA SURA------------#
    Documents_SURA = documentos[~documentos.Aseguradora.isin(['Sura','SURA ','SURAMERICANA SA ','SURAMERICANA','SURA']) == False]
    if Documents_SURA.shape[0] > 0:
        Documents_SURA = Documents_SURA.reset_index()
        del Documents_SURA['index']
        Base_Inicio_SURA,Base_Inicio_SURA_log = Carct_Basicas_SURA(Documents_SURA)
        Base_Riesgo_SURA,Base_Riesgo_SURA_log = Caract_Riesgo_Coberturas_SURA(Documents_SURA)
        Base_Genral_SURA_dos, Base_Genral_SURA_dos_log = Caract_Generales_SURA_Dos(Documents_SURA)
        Base_Genral_SURA, Base_Genral_SURA_log = Caract_Generales_SURA(Documents_SURA)

        Base_Genral_SURA_dos= pd.merge(Base_Genral_SURA, Base_Genral_SURA_dos, on = 'Nombre_Poliza')
        RESULTADOS_POLIZA_SURA = pd.concat([Base_Genral_SURA_dos, Base_Riesgo_SURA])
        RESULTADOS_POLIZA_SURA = pd.merge(Base_Inicio_SURA,RESULTADOS_POLIZA_SURA ,on= 'Nombre_Poliza')
        RESULTADOS_POLIZA_SURA = RESULTADOS_POLIZA_SURA.drop_duplicates()
        RESULTADOS_POLIZA_SURA = RESULTADOS_POLIZA_SURA.sort_values(by =['Nombre_Poliza','Riesgo_ID'] )
    
        #log_familia_sura
        Base_Genral_SURA_dos_log = pd.merge(Base_Genral_SURA_log, Base_Genral_SURA_dos_log, on = 'Nombre_Poliza')
        RESULTADOS_POLIZA_SURA_log = pd.merge(Base_Genral_SURA_dos_log, Base_Riesgo_SURA_log , on = 'Nombre_Poliza')
        RESULTADOS_POLIZA_SURA_log = pd.merge(Base_Inicio_SURA_log,RESULTADOS_POLIZA_SURA_log ,on= 'Nombre_Poliza')
        #Añade a la lista de familias:
        Lista_Familias.append(RESULTADOS_POLIZA_SURA)
        Lista_Familias_log.append( RESULTADOS_POLIZA_SURA_log )
        
    #------------FAMILIA PROVINCIA------------#
    Documents_PROVINCIA = documentos[documentos.Aseguradora == 'PROVINCIA SEGUROS']
    if Documents_PROVINCIA.shape[0] > 0:
        Documents_PROVINCIA = Documents_PROVINCIA.reset_index()
        del Documents_PROVINCIA['index']
        Base_Inicio_PROV,Base_Inicio_PROV_log = Carct_Basicas_PROVINCIA(Documents_PROVINCIA)
        Base_Riesgo_PROV,Base_Riesgo_PROV_log = Caract_Riesgos_Cobertura_PROVINCIA(Documents_PROVINCIA)
        Base_Generl_PROV,Base_Generl_PROV_log = Caract_Generales_PROVINCIA(Documents_PROVINCIA)
        
        RESULTADOS_POLIZA_PROV = pd.concat([Base_Riesgo_PROV, Base_Generl_PROV ])
        RESULTADOS_POLIZA_PROV = pd.merge(Base_Inicio_PROV, RESULTADOS_POLIZA_PROV,on= 'Nombre_Poliza')
        RESULTADOS_POLIZA_PROV = RESULTADOS_POLIZA_PROV.sort_values(by =['Nombre_Poliza','Riesgo_ID'] )

        #log_familia_provincia
        RESULTADOS_POLIZA_PROV_log = pd.merge(Base_Inicio_PROV_log, Base_Generl_PROV_log ,on= 'Nombre_Poliza')
        RESULTADOS_POLIZA_PROV_log = pd.concat([Base_Riesgo_PROV_log, RESULTADOS_POLIZA_PROV_log])
        #Añade a la lista de familias:
        Lista_Familias.append(RESULTADOS_POLIZA_PROV)
        Lista_Familias_log.append( RESULTADOS_POLIZA_PROV_log )
        
    #------------FAMILIA OTROS------------#
    Documents_OTRAS = documentos.loc[~documentos['Aseguradora'].isin(['PROVINCIA SEGUROS','HDI SEGUROS','Sura','SURA ','SURAMERICANA SA ','SURAMERICANA','SURA'])]
    if Documents_OTRAS.shape[0]>0:
        Documents_OTRAS = Documents_OTRAS.reset_index()
        del Documents_OTRAS['index']
        Base_Inicio_OTROS , Base_Inicio_OTROS_LOG = Carct_Basicas_OTROS(Documents_OTRAS)
        Base_Riesgos_OTROS ,Base_Riesgos_OTROS_LOG = Extraccion_riesgos(Documents_OTRAS)
        Base_Coberturas_OTROS , Base_Coberturas_OTROS_LOG = GENERAL_OTRAS_COBERTURAS(Documents_OTRAS)
        Base_Gneral_OTROS, Base_Gneral_OTROS_LOG = Genral_OTRAS_polizas(Documents_OTRAS)
        
        #Concatena y une las base----------#
        Base_Riesgos_OTROS = pd.merge(Base_Inicio_OTROS, Base_Riesgos_OTROS, on = 'Nombre_Poliza')
        Base_Gneral_OTROS = pd.merge(Base_Inicio_OTROS, Base_Gneral_OTROS, on = 'Nombre_Poliza')
        
        RESULTADOS_POLIZA_OTROS = pd.concat([Base_Gneral_OTROS, Base_Riesgos_OTROS])
        RESULTADOS_POLIZA_OTROS = pd.concat([RESULTADOS_POLIZA_OTROS, Base_Coberturas_OTROS ])
        RESULTADOS_POLIZA_OTROS = RESULTADOS_POLIZA_OTROS.sort_values(by = ['Nombre_Poliza','Riesgo_ID' ])
        
        #Concatena los logs ---------------#
        RESULTADOS_POLIZA_OTROS_log_uno = pd.merge(Base_Inicio_OTROS_LOG ,Base_Gneral_OTROS_LOG, on = 'Nombre_Poliza' )
        RESULTADOS_POLIZA_OTROS_log_dos = pd.merge(Base_Riesgos_OTROS_LOG ,Base_Coberturas_OTROS_LOG , on = 'Nombre_Poliza' )
        RESULTADOS_POLIZA_OTROS_log = pd.concat([ RESULTADOS_POLIZA_OTROS_log_uno , RESULTADOS_POLIZA_OTROS_log_dos])
        RESULTADOS_POLIZA_OTROS_log = RESULTADOS_POLIZA_OTROS_log.sort_values(by = ['Nombre_Poliza','Riesgo_ID' ])
        
        #Añade a la lista de familias:
        Lista_Familias.append(RESULTADOS_POLIZA_OTROS)
        Lista_Familias_log.append( RESULTADOS_POLIZA_OTROS_log )
        
    #----------------------------------------------------#
    #-------------UNIR TODOS LOS DOCUMENTOS-------------#
    if len(Lista_Familias) > 0:
        RESULTADOS_POLIZA = pd.concat(Lista_Familias)
        RESULTADOS_POLIZA = RESULTADOS_POLIZA.applymap(lambda x: re.sub(pattern='\s+', repl=' ', string = str(x)).strip().lower().title())
        RESULTADOS_POLIZA['Aseguradora'] = RESULTADOS_POLIZA['Aseguradora'].apply(lambda x: x.upper())
        #Reorganiza las columnas
        RESULTADOS_POLIZA = RESULTADOS_POLIZA[[
        #------------informacion general------------#
        'Nombre_Poliza',  'Aseguradora','Num_Poliza', 'Certificado',
       'Tipo_Poliza', 'Fecha_Inicio_Vigencia', 'Fecha_Fin_Vigencia',
        'Asegurado', 'ID_Asegurado', 'Tomador', 'ID_Tomador', 
        #'Beneficiario','ID_Beneficario',
        #------------informacion totales------------#
        'Moneda_Poliza', 'Total_Asegurable', 'Moneda_Total_Asegurable','Total_Asegurado','Moneda_Total_Asegurado', 'Total_Prima','Moneda_Prima','Participacion',
        #------------informacion Riesgo------------#
        'Riesgo_ID', 'Descrip_Riesg_1', 'Descrip_Riesg_2',  'Pais','Direccion_Riesgo','Division_Administrativa_1','Division_Administrativa_2', 'Division_Administrativa_3',
        'Codigo_Postal','Latitud','Longitud',
        #------------informacion Coberturas------------#
         'Cobertura', 'Prima_Cobertura','Valor_Cobert', 'Valor_Cobertura_%','Valor_Cobertura_Lucro_cesante_(dias)','Base_Valor_Cobertura_%', 'Moneda_Prima_Cobertura',
        #------------informacion deducibles------------#
        'Deducibles_raw','Deducible_$', 'Deducible_%','Base_Deducible_%','Deducible_Lucro_Cesante_(dias)', 'Deducible_Min_$',  'Moneda_Deducible_$', 'Moneda_Deducible_Min_$',
        #------------Limites y Lucro_cesante------------#
        'Limites_raw'
        ]]
    else:
        RESULTADOS_POLIZA = pd.DataFrame({
        #------------informacion general------------#
        'Nombre_Poliza' :['No se encuentra'],  'Aseguradora':['No se encuentra'],'Num_Poliza':['No se encuentra'], 'Certificado':['No se encuentra'],
       'Tipo_Poliza':['No se encuentra'], 'Fecha_Inicio_Vigencia':['No se encuentra'], 'Fecha_Fin_Vigencia':['No se encuentra'],
        'Asegurado':['No se encuentra'], 'ID_Asegurado':['No se encuentra'], 'Tomador':['No se encuentra'], 'ID_Tomador':['No se encuentra'], 
        #'Beneficiario','ID_Beneficario',
        #------------informacion totales------------#
        'Moneda_Poliza':['No se encuentra'], 'Total_Asegurable':['No se encuentra'], 'Moneda_Total_Asegurable':['No se encuentra'],'Total_Asegurado':['No se encuentra'],'Moneda_Total_Asegurado':['No se encuentra'], 
        'Total_Prima':['No se encuentra'],'Moneda_Prima':['No se encuentra'],'Participacion':['No se encuentra'],
        #------------informacion Riesgo------------#
        'Riesgo_ID':['No se encuentra'], 'Descrip_Riesg_1':['No se encuentra'], 'Descrip_Riesg_2':['No se encuentra'],  'Pais':['No se encuentra'],'Direccion_Riesgo':['No se encuentra'],
        'Division_Administrativa_1':['No se encuentra'],'Division_Administrativa_2':['No se encuentra'], 'Division_Administrativa_3':['No se encuentra'],
        'Codigo_Postal':['No se encuentra'],'Latitud':['No se encuentra'],'Longitud':['No se encuentra'],
        #------------informacion Coberturas------------#
        'Cobertura':['No se encuentra'], 'Prima_Cobertura':['No se encuentra'],'Valor_Cobert':['No se encuentra'], 'Valor_Cobertura_%':['No se encuentra'],'Valor_Cobertura_Lucro_cesante_(dias)':['No se encuentra'],
        'Base_Valor_Cobertura_%':['No se encuentra'], 'Moneda_Prima_Cobertura':['No se encuentra'],
        #------------informacion deducibles------------#
        'Deducibles_raw':['No se encuentra'],'Deducible_$':['No se encuentra'], 'Deducible_%':['No se encuentra'],'Base_Deducible_%':['No se encuentra'],
        'Deducible_Lucro_Cesante_(dias)':['No se encuentra'], 'Deducible_Min_$':['No se encuentra'],  'Moneda_Deducible_$':['No se encuentra'], 'Moneda_Deducible_Min_$':['No se encuentra'],
        #------------Limites y Lucro_cesante------------#
        'Limites_raw':['No se encuentra'] })

    if len(Lista_Familias_log)> 0:
        RESULTADOS_POLIZA_LOG = pd.concat(Lista_Familias_log)
        RESULTADOS_POLIZA_LOG = RESULTADOS_POLIZA_LOG.applymap(lambda x: re.sub(pattern='\s+', repl=' ', string = str(x)).strip().lower().title())
        #Organiza las caracteristiscas del plot
        ####### ---------- Quita dobles espacios y espacios al final ---------- ####### 
        RESULTADOS_POLIZA_LOG = RESULTADOS_POLIZA_LOG[[
        #------------informacion general------------#
        'Nombre_Poliza',  'Aseguradora','Num_Poliza', 'Certificado',
       'Tipo_Poliza', 'Fecha_Inicio_Vigencia', 'Fecha_Fin_Vigencia',
        'Asegurado', 'ID_Asegurado', 'Tomador', 'ID_Tomador', 
        #'Beneficiario','ID_Beneficario',
        #------------informacion totales------------#
        'Moneda_Poliza', 'Total_Asegurable', 'Moneda_Total_Asegurable','Total_Asegurado','Moneda_Total_Asegurado', 'Total_Prima','Moneda_Prima','Participacion',
        #------------informacion Riesgo------------#
        'Riesgo_ID', 'Descrip_Riesg_1', 'Descrip_Riesg_2',  'Pais','Direccion_Riesgo','Division_Administrativa_1','Division_Administrativa_2', 'Division_Administrativa_3',
        'Codigo_Postal','Latitud','Longitud',
        #------------informacion Coberturas------------#
         'Cobertura', 'Prima_Cobertura','Valor_Cobert', 'Valor_Cobertura_%','Valor_Cobertura_Lucro_cesante_(dias)','Base_Valor_Cobertura_%', 'Moneda_Prima_Cobertura',
        #------------informacion deducibles------------#
        'Deducibles_raw','Deducible_$', 'Deducible_%','Base_Deducible_%','Deducible_Lucro_Cesante_(dias)', 'Deducible_Min_$',  'Moneda_Deducible_$', 'Moneda_Deducible_Min_$',
        #------------Limites y Lucro_cesante------------#
        'Limites_raw'
        ]]
    else:
        RESULTADOS_POLIZA_LOG = pd.DataFrame({
        #------------informacion general------------#
        'Nombre_Poliza' :['No se encuentra'],  'Aseguradora':['No se encuentra'],'Num_Poliza':['No se encuentra'], 'Certificado':['No se encuentra'],
       'Tipo_Poliza':['No se encuentra'], 'Fecha_Inicio_Vigencia':['No se encuentra'], 'Fecha_Fin_Vigencia':['No se encuentra'],
        'Asegurado':['No se encuentra'], 'ID_Asegurado':['No se encuentra'], 'Tomador':['No se encuentra'], 'ID_Tomador':['No se encuentra'], 
        #'Beneficiario','ID_Beneficario',
        #------------informacion totales------------#
        'Moneda_Poliza':['No se encuentra'], 'Total_Asegurable':['No se encuentra'], 'Moneda_Total_Asegurable':['No se encuentra'],'Total_Asegurado':['No se encuentra'],'Moneda_Total_Asegurado':['No se encuentra'], 
        'Total_Prima':['No se encuentra'],'Moneda_Prima':['No se encuentra'],'Participacion':['No se encuentra'],
        #------------informacion Riesgo------------#
        'Riesgo_ID':['No se encuentra'], 'Descrip_Riesg_1':['No se encuentra'], 'Descrip_Riesg_2':['No se encuentra'],  'Pais':['No se encuentra'],'Direccion_Riesgo':['No se encuentra'],
        'Division_Administrativa_1':['No se encuentra'],'Division_Administrativa_2':['No se encuentra'], 'Division_Administrativa_3':['No se encuentra'],
        'Codigo_Postal':['No se encuentra'],'Latitud':['No se encuentra'],'Longitud':['No se encuentra'],
        #------------informacion Coberturas------------#
        'Cobertura':['No se encuentra'], 'Prima_Cobertura':['No se encuentra'],'Valor_Cobert':['No se encuentra'], 'Valor_Cobertura_%':['No se encuentra'],'Valor_Cobertura_Lucro_cesante_(dias)':['No se encuentra'],
        'Base_Valor_Cobertura_%':['No se encuentra'], 'Moneda_Prima_Cobertura':['No se encuentra'],
        #------------informacion deducibles------------#
        'Deducibles_raw':['No se encuentra'],'Deducible_$':['No se encuentra'], 'Deducible_%':['No se encuentra'],'Base_Deducible_%':['No se encuentra'],
        'Deducible_Lucro_Cesante_(dias)':['No se encuentra'], 'Deducible_Min_$':['No se encuentra'],  'Moneda_Deducible_$':['No se encuentra'], 'Moneda_Deducible_Min_$':['No se encuentra'],
        #------------Limites y Lucro_cesante------------#
        'Limites_raw':['No se encuentra'] })
        
    #Pone espacios en blanco:
    RESULTADOS_POLIZA  = RESULTADOS_POLIZA .replace('Nan', '')  
    RESULTADOS_POLIZA_LOG  = RESULTADOS_POLIZA_LOG .replace('Nan', '') 
    return(RESULTADOS_POLIZA, RESULTADOS_POLIZA_LOG)
    


    
#-------------------------------------------------------------#
#----- 4 Carga algunos diccionarios ---------------------------#
#-------------------------------------------------------------#    


#Carga diccionario de Aseguradoras #
tipo_aseguradoras = pd.read_excel(path+ 'Datos/Informacion/Diccionario aseguradora.xlsx')
Tipo_Aseguradora = tipo_aseguradoras.Nombre_aseguradora.tolist()
Tipo_Aseguradora = Espanhol_Palabras(Tipo_Aseguradora)

#Carga diccionario de Coberturas#
matriz_coberturas = pd.read_excel(path+ 'Datos/Informacion/Diccionario_Coberturas.xlsx')
#convertir a una lista
Listas_coberturas = matriz_coberturas.values.tolist()
result =[]
result.append([item for sub_list in Listas_coberturas for item in sub_list])
result = result[0]
result = [item for item in result if pd.isna(item) == False]
resultados_coberturas = Espanhol_Palabras(result)
Coberturas  = '('+'|'.join(resultados_coberturas) +')'
del Listas_coberturas, matriz_coberturas, result, resultados_coberturas