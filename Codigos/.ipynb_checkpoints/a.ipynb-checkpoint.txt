{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!pip3 install folium\n",
    "import folium\n",
    "import pandas as pd \n",
    "import os\n",
    "import functools\n",
    "import re\n",
    "import json\n",
    "import folium\n",
    "from folium import plugins\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "#!pip3 install seaborn\n",
    "import seaborn as sns\n",
    "import os\n",
    "import numpy as np\n",
    "%matplotlib inline\n",
    "### ---- Directorio ---- ###\n",
    "os.chdir(\"/Users/germangonzalez/Dropbox (Quantil)/Movendo/Datos/\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "geoj = {\n",
    "  'type': 'FeatureCollection',\n",
    "  'features':\n",
    "  {\n",
    "    'type': 'Feature',\n",
    "    'geometry': {\n",
    "      'type': 'Polygon',\n",
    "      'coordinates': [\n",
    "          [ [100.0, 0.0], [101.0, 0.0], [101.0, 1.0],\n",
    "            [100.0, 1.0], [100.0, 0.0] ],\n",
    "          [ [101.0, 0.0], [102.0, 0.0], [102.0, 1.0],\n",
    "            [101.0, 1.0], [101.0, 0.0] ],\n",
    "          [ [100.0, 1.0], [101.0, 1.0], [101.0, 2.0],\n",
    "            [100.0, 2.0], [100.0, 1.0] ],\n",
    "          [ [101.0, 1.0], [102.0, 1.0], [102.0, 2.0],\n",
    "            [101.0, 2.0], [101.0, 1.0] ],\n",
    "          ]\n",
    "      }\n",
    "    }\n",
    "  }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/usr/local/lib/python3.6/site-packages/pysal/__init__.py:65: VisibleDeprecationWarning: PySAL's API will be changed on 2018-12-31. The last release made with this API is version 1.14.4. A preview of the next API version is provided in the `pysal` 2.0 prelease candidate. The API changes and a guide on how to change imports is provided at https://pysal.org/about\n",
      "  ), VisibleDeprecationWarning)\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import pysal as ps\n",
    "import shapely.geometry as shg\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords= geoj[\"features\"][\"geometry\"][\"coordinates\"]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[101.0, 0.0], [102.0, 0.0], [102.0, 1.0], [101.0, 1.0], [101.0, 0.0]]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "coords[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "points_as_array = np.array(points) # I am not sure if this is mandatory in recent versions of pysal\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 182,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = json.load(open('Mapas/Melbourne/query.json'))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 186,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords = [data[\"features\"][j][\"geometry\"][\"coordinates\"][0] for j in range(0,10)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 191,
   "metadata": {},
   "outputs": [],
   "source": [
    "polys = [\n",
    "    ps.cg.asShape(shg.Polygon(c))\n",
    "    for c in coords\n",
    "]\n",
    "\n",
    "#### ---- Centroides ---- ####\n",
    "points = [\n",
    "    p.centroid\n",
    "    for p in polys\n",
    "]\n",
    "\n",
    "points_as_array = np.array(points) # I am not sure if this is mandatory in recent versions of pysal\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 192,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[(-73.79071675243046, 40.684991314796086),\n",
       " (-73.73880107562898, 40.71231927104023),\n",
       " (-73.88777310378187, 40.75266857302103),\n",
       " (-73.87416899132478, 40.7445643600549),\n",
       " (-73.99137589519722, 40.60213291030624),\n",
       " (-73.97874244140507, 40.624854566144094),\n",
       " (-74.00159482717962, 40.62415426960679),\n",
       " (-73.89435731027966, 40.68584343565107),\n",
       " (-73.92124214416113, 40.64739606411039),\n",
       " (-73.77277700573016, 40.72310204189061)]"
      ]
     },
     "execution_count": 192,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 91,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shapely.geometry import Point\n",
    "from shapely.geometry.polygon import Polygon\n",
    "from shapely.geometry import LineString"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 165,
   "metadata": {},
   "outputs": [],
   "source": [
    "import shapely.speedups\n",
    "shapely.speedups.enable()\n",
    "pip_mask = data.within(southern.loc[0, 'geometry'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 143,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'list' object has no attribute '_geom'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-143-a847b618ba24>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mnearest_geoms\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mnearest_points\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0morig\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mPoligonos\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m/usr/local/lib/python3.6/site-packages/shapely/ops.py\u001b[0m in \u001b[0;36mnearest_points\u001b[0;34m(g1, g2)\u001b[0m\n\u001b[1;32m    260\u001b[0m     \u001b[0mThe\u001b[0m \u001b[0mpoints\u001b[0m \u001b[0mare\u001b[0m \u001b[0mreturned\u001b[0m \u001b[0;32min\u001b[0m \u001b[0mthe\u001b[0m \u001b[0msame\u001b[0m \u001b[0morder\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0mthe\u001b[0m \u001b[0minput\u001b[0m \u001b[0mgeometries\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    261\u001b[0m     \"\"\"\n\u001b[0;32m--> 262\u001b[0;31m     \u001b[0mseq\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mlgeos\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mmethods\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m'nearest_points'\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mg1\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_geom\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mg2\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_geom\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    263\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mseq\u001b[0m \u001b[0;32mis\u001b[0m \u001b[0;32mNone\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    264\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0mg1\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mis_empty\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'list' object has no attribute '_geom'"
     ]
    }
   ],
   "source": [
    "nearest_geoms = nearest_points(orig, Poligonos)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 142,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(<shapely.geometry.point.Point object at 0x125865710>, <shapely.geometry.point.Point object at 0x125865e80>)\n"
     ]
    }
   ],
   "source": [
    "print(nearest_geoms)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 187,
   "metadata": {},
   "outputs": [],
   "source": [
    "Poligonos = [LineString(c).union(Polygon(c)) for c in coords]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 188,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shapely.geometry.multipolygon import MultiPolygon\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 190,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mapa =  MultiPolygon(Poligonos)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 194,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'list' object has no attribute 'within'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-194-853231dc5f15>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0;34m[\u001b[0m\u001b[0;34m-\u001b[0m\u001b[0;36m73.79071675243046\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m40.684991314796086\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mwithin\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mMapa\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m: 'list' object has no attribute 'within'"
     ]
    }
   ],
   "source": [
    "[-73.79071675243046, 40.684991314796086].within(Mapa)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 172,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[<shapely.geometry.polygon.Polygon at 0x1239ad518>]"
      ]
     },
     "execution_count": 172,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Poligonos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 130,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<map at 0x1269eb3c8>"
      ]
     },
     "execution_count": 130,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "map(Point(-73.79071675,40.68499131),Poligonos)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 115,
   "metadata": {},
   "outputs": [],
   "source": [
    "mapa = map(lambda x: x.contains(Point(-73.77277700573016, 40.72310204189061)),Poligonos)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 116,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[False, False, False, False, False, False, False, False, False, True]"
      ]
     },
     "execution_count": 116,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(mapa)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{0: [9, 1],\n",
       " 1: [9, 0],\n",
       " 2: [3, 7],\n",
       " 3: [2, 7],\n",
       " 4: [6, 5],\n",
       " 5: [6, 4],\n",
       " 6: [5, 4],\n",
       " 7: [8, 3],\n",
       " 8: [7, 5],\n",
       " 9: [1, 0]}"
      ]
     },
     "execution_count": 85,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "wnn2 = ps.knnW_from_array(points_as_array, 2) # deprecated and removed soon (or later), and replaced by KNN.from_array\n",
    "wnn2.transform = 'r' # make wnn2 be row-stochastic\n",
    "wnn2.neighbors"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
